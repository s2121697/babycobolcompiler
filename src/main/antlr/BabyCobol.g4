parser grammar BabyCobol;

options {
    tokenVocab = BabyCobolVocabular;
}

program
    : identificationDivision (dataDivision)? (procedureDivision)? EOF
    ;

identificationDivision
    : IDENTIFICATION DIVISION DOT (divisionName DOT divisionName DOT)+
    ;

dataDivision
    : DATA DIVISION DOT (dataEntry)+
    ;

procedureDivision
    : PROCEDURE DIVISION DOT (sentence+)? paragraph*
    ;

divisionName
    : ID
    | NO_DOTS
    ;

sentence
    : stat+ DOT
    ;

paragraph
    : identifier DOT sentence+
    ;

dataEntry
    : INT_LIT identifier (pictureClause | likeClause)? occursClause? DOT
    ;

pictureClause
    : PICTURE IS representation
    ;

likeClause
    : LIKE identifier
    ;

occursClause
    :  OCCURS n=INT_LIT TIMES
    ;

representation
    : ID
    | INT_LIT
    ;

stat
    : ACCEPT identifier+                                                                #acceptStat
    | ADD left=atomic TO right=atomic (GIVING identifier)?                              #addStat
    | SUBTRACT left=atomic FROM right=atomic (GIVING identifier)?                       #subtractStat
    | MULTIPLY left=atomic BY right=atomic (GIVING identifier)?                         #multiplyStat
    | DISPLAY (dispOptions)+? (WITH NO ADVANCING)?                                      #displayStat
    | DIVIDE left=atomic INTO right=atomic (GIVING identifier)? (REMAINDER identifier)? #divideStat
    | STOP                                                                              #stopStat
    | IF boolExpr THEN ifStats+=stat+ (ELSE elseStats+=stat+?)? END                     #conditionalStat
    | EVALUATE anyExpr whenBlock*? END                                                  #evaluateStat
    | GO_TO identifier                                                                  #goToStat
    | PERFORM startProc=identifier (THROUGH endProc=identifier)? (atomic TIMES)?        #performStat
    ;

anyExpr
    : arithExpr
    | boolExpr
    ;

whenBlock
    : WHEN (OTHER | atomic+?) stat+
    ;

dispOptions
    : atomic (DELIMITED BY (SIZE | SPACE | literal))?
    ;

boolExpr
    : TRUE
    | FALSE
    | arithExpr compOp arithExpr
    | NOT boolExpr
    | boolExpr boolOp boolExpr
    ;

arithExpr
    : arithExpr POW_OP arithExpr                #powerExpr
    | arithExpr (MUL_OP | DIV_OP) arithExpr     #mulDivExpr
    | arithExpr (ADD_OP | SUB_OP) arithExpr     #addSubExpr
    | atomic                                    #arithAtomic
    ;

atomic
    : literal       #literalAtomic
    | identifier    #idAtomic
    ;

literal
    : INT_LIT   #intLit
    | NUM_LIT   #numLit
    | TXT_LIT   #txtLit
    | TRUE      #trueLit
    | FALSE     #falseLit
    ;

identifier
    : ACCEPT
    | ADD
    | ADVANCING
    | AND
    | BY
    | DATA
    | DELIMITED
    | DISPLAY
    | DIVIDE
    | DIVISION
    | ELSE
    | END
    | EVALUATE
    | FALSE
    | FROM
    | GIVING
    | IDENTIFICATION
    | IF
    | IS
    | MULTIPLY
    | NO
    | NOT
    | OCCURS
    | OF
    | OTHER
    | OR
    | PERFORM
    | PROCEDURE
    | PICTURE
    | SIZE
    | SPACE
    | STOP
    | SUBTRACT
    | THEN
    | THROUGH
    | TO
    | TRUE
    | WITH
    | WHEN
    | XOR
    | identifier (OF identifier)+
    | identifier INDEX
    | ID
    ;

boolOp
    : AND
    | OR
    | XOR
    ;

compOp
    : EQ
    | GT
    | LT
    | GE
    | LE
    ;
