package com.gitlab.legoatoom.babycobolcompiler.ir

import com.gitlab.legoatoom.babycobolcompiler.checker.SymbolTable
import com.gitlab.legoatoom.babycobolcompiler.util.whenNotNull
import org.intellij.lang.annotations.Language

data class IRProgram(
    private val symbolTable: SymbolTable,
    private val procedureDivision: IRProcedureDivision?,
    private val paragraphs: List<IRParagraph> = emptyList()
) : IRHelper {

    /**
     * Converts the list of stats to lines of kotlin code.
     */
    override fun convert(): String {
        // We declare all variables (data entries) at the top of the script. Variables are global in BabyCobol and thus
        // there also global in the generated Kotlin script.
        val declVars = this.symbolTable.makeVarDeclarations()

        // At the end of the script we insert the Picture class. All variables are instances of this class. It has
        // toString, getValue and setValue methods to handle updates and formatting.
        @Language("kotlin")
        val boilerplate: String = """
            open class Picture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) {
                private var value: String = padSymbol.toString().repeat(len)
                
                open fun getValue(): Any { return this.value }
                
                override fun toString(): String { return this.value }
                
                fun setValue(newVal: String) {
                    // Check length of newVal. Pad to left with spaces / 0's or cut left most part of string.
                    var updatedVal: String = newVal
                    if (newVal.length < len) {
                        updatedVal = newVal.padStart(len, padSymbol)
                    } else if (newVal.length > len) {
                        updatedVal = newVal.takeLast(len)
                    }

                    // Check if value correspond with regex
                    if (regex.matches(updatedVal)) this.value = updatedVal
                    else {
                        val errMessage = "Cannot assign " + updatedVal + " to data entry " + this.name
                        throw Exception(errMessage)
                    }
                }
            }
            
            class IntPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
                override fun getValue(): java.math.BigInteger {
                    return super.getValue().toString().toBigInteger()
                }
            }
            
            class StringPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
                override fun getValue(): String {
                    return super.getValue().toString()
                }
            }
            
            class GoToReturn : Throwable()
            class ExitScript : Throwable()
        """.trimIndent()
        val scope = "val scope : java.util.Stack<Set<String>> = java.util.Stack()"

        val paragraphsCall: String = symbolTable.getParagraphs().joinToString(separator = ";", prefix = "   ") { s -> IRIdentifier(s).convert() + "()" }

        return buildString {
            appendLine("// Scope Stack")
            appendLine(scope)
            appendLine("\n// Declare Variables")
            append(declVars)
            whenNotNull(procedureDivision){
                appendLine("\n// Begin Code")
                appendLine("try {")
                append(it.convert())
                appendLine("\n\n// Paragraphs Call")
                append(paragraphsCall)
                appendLine("\n} catch (ignore: ExitScript) {}")
                appendLine("\n\n// Paragraphs Definition")
                append(paragraphs.joinToString(";", transform = IRParagraph::makeParagraph))

            }
            appendLine("\n\n// Boilerplate")
            append(boilerplate)
        }
    }
}
