package com.gitlab.legoatoom.babycobolcompiler.ir

class IRGoTo(private val procedureName: IRIdentifier, private val paragraphs: List<String>) : IRStat() {
    override fun convert(): String {
        var firstFound = false

        val procedureNameConverted = procedureName.convert()
        val code = paragraphs.filter { paragraph: String ->
            firstFound || (IRIdentifier(paragraph).convert() == procedureNameConverted).also {
                if (it) firstFound = true //short-circuit evaluation prevents firstFound to become false again. }
            }
        }.joinToString(separator = ";") { "${IRIdentifier(it).convert()}();if (scope.peek().contains(\"$it\")) { throw GoToReturn()}" }

        return "$code;${IRStop().convert()}"
    }
}
