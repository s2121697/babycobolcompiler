package com.gitlab.legoatoom.babycobolcompiler.ir

//check for other to be last in checker.
class IREvaluate(
    private val anyExpr: IRAnyExpr,
    private val whenBlocks: List<IRWhenBlock>
) : IRStat() {
    override fun convert(): String {
        val convertedStats = whenBlocks.joinToString("\n") { it.convert().prependIndent() }.trimStart()
        //If there are no 'when's, we just run it since it might be important.
        if (convertedStats.isEmpty()) return anyExpr.convert()

        return """
           |when (${anyExpr.convert()}) {
           |    $convertedStats
           |}
        """.trimMargin()
    }
}
