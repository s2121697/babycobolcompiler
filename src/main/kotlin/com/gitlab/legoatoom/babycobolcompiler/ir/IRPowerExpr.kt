package com.gitlab.legoatoom.babycobolcompiler.ir

class IRPowerExpr(
    private val left: IRArithExpr,
    private val right: IRArithExpr
) : IRArithExpr {
    override fun convert(): String {
        // Use the pow()-function on BigIntegers for powers. The function expects an int as exponent, so the BigInteger
        // needs to be converted to an integer here.
        return "${left.convert()}.pow(${right.convert()}.toInt())"
    }
}