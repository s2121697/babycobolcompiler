package com.gitlab.legoatoom.babycobolcompiler.ir

class IRIndexedIdentifier(
    private var name: String,
    private var indexString: String
) : IRAtomic {
    override fun convert(): String {
        // Simply return the received name and indexString.
        return "$name$indexString"
    }
}