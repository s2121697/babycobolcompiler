package com.gitlab.legoatoom.babycobolcompiler.ir

import java.math.BigInteger

sealed class IRLiteral : IRAtomic {


    // Int literals are represented by BigIntegers as not to constrain the value.
    // See https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/java.math.-big-integer/.
    class IRIntLit(private val i: BigInteger) : IRLiteral() {
        override fun convert(): String {
            return "$i"
        }
    }

    class IRTxtLit(private val s: String) : IRLiteral() {
        override fun convert(): String {
            return s
        }
    }

    object IRTrueLit : IRLiteral() {
        override fun convert(): String = IRBoolExpr(trueLiteral = true).convert()
    }

    object IRFalseLit : IRLiteral() {
        override fun convert(): String = IRBoolExpr(falseLiteral = true).convert()
    }
}


