package com.gitlab.legoatoom.babycobolcompiler.ir

class IRParagraph(
    private val identifier: IRIdentifier,
    private val sentences: List<IRSentence>
) : IRHelper {
    override fun convert(): String {
        return ""
    }

    fun makeParagraph(): String {
        val name: String = identifier.convert()
        // A paragraph is normally run.
        return """
            |fun $name(){
            |   ${sentences.joinToString("\n   ", transform = IRSentence::convert)}
            |}
        """.trimMargin()
    }
}
