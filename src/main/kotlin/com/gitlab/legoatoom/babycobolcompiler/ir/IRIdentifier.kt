package com.gitlab.legoatoom.babycobolcompiler.ir

import java.util.*

class IRIdentifier(
    private var name: String,
    private var indexString: String = "",
    private var unresolvedKeyword: Boolean = false,
) : IRAtomic {
    // Probably other info here later too, such as picture clause. (aka type)
    override fun convert(): String {
        if (unresolvedKeyword) {
            return "\"${name.uppercase()}\""
        }

        if (indexString.isNotEmpty()) {
            return "_$name$indexString"
        }

        return "_" + name.lowercase(Locale.getDefault())
    }
}
