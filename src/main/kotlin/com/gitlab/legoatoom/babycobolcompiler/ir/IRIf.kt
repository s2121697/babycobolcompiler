package com.gitlab.legoatoom.babycobolcompiler.ir

class IRIf(
    private val boolExpr: IRBoolExpr,
    private val ifStats: List<IRStat>,
    private val elseStats: List<IRStat>
) : IRStat() {

    override fun convert(): String {
        // No else-statement
        if (elseStats.isEmpty()) {
            // Using this indentation because the \n will mess it up otherwise, and it will be very hard to compare
            // in tests.
            return """
                |if (${boolExpr.convert()}) {
                |    ${ifStats.joinToString("\n", transform = IRStat::convert)}
                |}
            """.trimMargin()
        }

        return """
            |if (${boolExpr.convert()}) {
            |    ${ifStats.joinToString("\n", transform = IRStat::convert)}
            |} else {
            |    ${elseStats.joinToString("\n", transform = IRStat::convert)}
            |}
        """.trimMargin()
    }
}
