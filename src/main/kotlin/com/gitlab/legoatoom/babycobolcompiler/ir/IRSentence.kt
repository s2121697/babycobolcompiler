package com.gitlab.legoatoom.babycobolcompiler.ir

class IRSentence(private val stats: List<IRStat>) : IRHelper {
    override fun convert(): String {
        return stats.map(IRStat::convert).filter(String::isNotBlank).joinToString("\n")
    }
}
