package com.gitlab.legoatoom.babycobolcompiler.ir

class IRDisplay(private val dispExprs: List<IRDispExpr>, private val withNoAdvancing: Boolean) : IRStat() {
    override fun convert(): String {
        // Print all atomics present with separate Kotlin print statements.
        return if (withNoAdvancing) {
            dispExprs.joinToString(separator = "\n") { "print(${it.convert()})" }
        } else {
            dispExprs.joinToString(separator = "\n") { "println(${it.convert()})" }
        }
    }
}
