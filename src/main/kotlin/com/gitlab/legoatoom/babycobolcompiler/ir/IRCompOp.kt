package com.gitlab.legoatoom.babycobolcompiler.ir

class IRCompOp(
    private val eq: Boolean,
    private val gt: Boolean,
    private val lt: Boolean,
    private val ge: Boolean,
    private val le: Boolean
) : IRStat() {

    override fun convert(): String {
        return if (eq) "=="
        else if (gt) ">"
        else if (lt) "<"
        else if (ge) ">="
        else "<="
    }
}