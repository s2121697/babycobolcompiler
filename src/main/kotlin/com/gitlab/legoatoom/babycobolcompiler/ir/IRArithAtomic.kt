package com.gitlab.legoatoom.babycobolcompiler.ir

class IRArithAtomic(private val atomic: IRAtomic, private val isId: Boolean) : IRArithExpr {
    override fun convert(): String {
        // Since all arithmetic expression are on BigIntegers (the variables are BigIntegers), we need to convert the
        // atomic to a BigInteger in the correct way.
        return if (isId) "${atomic.convert()}.getValue()" else "${atomic.convert()}.toBigInteger()"
    }
}