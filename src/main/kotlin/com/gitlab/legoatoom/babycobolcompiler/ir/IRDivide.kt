package com.gitlab.legoatoom.babycobolcompiler.ir

class IRDivide(
    private val atomic1: IRAtomic,
    private val atomic2: IRAtomic,
    private val givingIdentifier: IRIdentifier? = null,
    private val givingRemainder: IRIdentifier? = null,
    private val atomic1IsId: Boolean,
    private val atomic2IsId: Boolean
) : IRStat() {


    override fun convert(): String {
        val leftOp = atomic1.convert()
        val rightOp = atomic2.convert()

        // Get value of atomics. Mathematical operations are done on BigIntegers.
        val leftVal = if (atomic1IsId) "$leftOp.getValue()" else "$leftOp.toBigInteger()"
        val rightVal = if (atomic2IsId) "$rightOp.getValue()" else "$rightOp.toBigInteger()"

        //Checking should be done in the Checker class.
        return if (givingIdentifier != null && givingRemainder != null) {

            val id = givingIdentifier.convert()
            val id2 = givingRemainder.convert()
            "$id.setValue(($leftVal.div($rightVal)).toString())" + "\n" +
                    "$id2.setValue(($leftVal.mod($rightVal)).toString())"

        } else if (givingIdentifier != null && givingRemainder == null) {
            val id = givingIdentifier.convert()
            "$id.setValue(($leftVal.div($rightVal)).toString())"

        } else if(givingRemainder != null){
            val id = givingRemainder.convert()
            "$id.setValue(($leftVal.mod($rightVal)).toString())"
        } else {
            "$leftOp.setValue(($leftVal.div($rightVal)).toString())"
        }

    }

}
