package com.gitlab.legoatoom.babycobolcompiler.ir

class IRBoolOp(
    private val and: Boolean,
    private val xor: Boolean,
    private val or: Boolean
) : IRStat() {

    override fun convert(): String {
        return if (and) "&&"
        else if (xor) "xor" // Kotlin infix operator on Booleans
        else "||"
    }
}