package com.gitlab.legoatoom.babycobolcompiler.ir

class IRProcedureDivision(
    private val sentences: List<IRSentence> = emptyList(),
    private val paragraphs: List<IRParagraph> = emptyList()
) : IRHelper {
    override fun convert(): String {
        // Sentences are global statements, paragraphs are functions in the Kotlin script.
        return """
${sentences.map(IRSentence::convert).filter(String::isNotBlank).joinToString("\n")}

${paragraphs.map(IRParagraph::convert).filter(String::isNotBlank).joinToString("\n")}
        """.trimIndent()
    }
}
