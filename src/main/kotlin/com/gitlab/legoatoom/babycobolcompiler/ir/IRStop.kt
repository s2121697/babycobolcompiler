package com.gitlab.legoatoom.babycobolcompiler.ir

class IRStop : IRStat() {

    override fun convert(): String {
        return "throw ExitScript()"
    }
}
