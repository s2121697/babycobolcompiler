package com.gitlab.legoatoom.babycobolcompiler.ir

interface IRHelper {
    fun convert(): String
}
