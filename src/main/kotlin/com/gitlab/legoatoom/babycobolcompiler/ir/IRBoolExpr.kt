package com.gitlab.legoatoom.babycobolcompiler.ir

class IRBoolExpr(
    private val trueLiteral: Boolean = false,
    private val falseLiteral: Boolean = false,
    private val negation: Boolean = false,
    private val boolOperator: IRBoolOp? = null,
    private val compOperator: IRCompOp? = null,
    private val boolExprs: List<IRBoolExpr> = emptyList(),
    private val arithExprs: List<IRArithExpr> = emptyList()
) : IRAnyExpr {

    // Boolean expression can be a TRUE/FALSE literal, a comparison between arithmetic expressions, a negation of a
    // Boolean expression or a Boolean operation on two Boolean expressions.
    override fun convert(): String {
        return if (trueLiteral) "true"
        else if (falseLiteral) "false"
        else if (compOperator != null) {
            "${arithExprs[0].convert()} ${compOperator.convert()} ${arithExprs[1].convert()}"
        } else if (negation) "!${boolExprs[0].convert()}"
        else {
            "${boolExprs[0].convert()} ${boolOperator?.convert()} ${boolExprs[1].convert()}"
        }
    }
}
