package com.gitlab.legoatoom.babycobolcompiler.ir

class IRAdd(
    private val atomic1: IRAtomic,
    private val atomic2: IRAtomic,
    private val givingIdentifier: IRIdentifier? = null,
    private val atomic1IsId: Boolean,
    private val atomic2IsId: Boolean
) : IRStat() {

    override fun convert(): String {
        val leftOp = atomic1.convert()
        val rightOp = atomic2.convert()

        // Get value of atomics. Mathematical operations are done on BigIntegers.
        val leftVal = if (atomic1IsId) "$leftOp.getValue()" else "$leftOp.toBigInteger()"
        val rightVal = if (atomic2IsId) "$rightOp.getValue()" else "$rightOp.toBigInteger()"

        // Checking should be done in the Checker class.
        return if (givingIdentifier == null) {
            "$rightOp.setValue(($leftVal.plus($rightVal)).toString())"
        } else {
            val id = givingIdentifier.convert()
            "$id.setValue(($leftVal.plus($rightVal)).toString())"
        }
    }
}
