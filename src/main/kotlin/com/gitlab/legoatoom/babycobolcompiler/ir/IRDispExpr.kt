package com.gitlab.legoatoom.babycobolcompiler.ir

enum class DelimiterType {
    SIZE, SPACE, NONE
}

class IRDispExpr(
    private val atomic: IRAtomic,
    private val delimiter: DelimiterType = DelimiterType.NONE,
    private val literal: IRLiteral? = null
) : IRStat() {

    // NOTE: both atomic.convert() and literal.convert() return Strings surrounded double quotes. The
    // .drop(1).dropLast(1) removes the first and last character from the Strings.
    override fun convert(): String {
        if (literal != null) {
            var splitRes: String = when (literal) {
                is IRLiteral.IRTxtLit -> {
                    // Lazily split string, only first part is computed.
                    atomic.convert().splitToSequence(literal.convert().drop(1).dropLast(1)).first()
                }
                else -> {
                    // Lazily split string, only first part is computed.
                    atomic.convert().splitToSequence(literal.convert()).first()
                }
            }

            // Add double quote to close the string properly, if last is not a double quote but first is.
            if (splitRes.first() == '"' && splitRes.last() != '"') splitRes += '"'

            return splitRes
        }

        return when (delimiter) {
            DelimiterType.SIZE -> atomic.convert().take(atomic.convert().length)
            DelimiterType.SPACE -> '"' + atomic.convert().drop(1).dropLast(1).trim() + '"'
            else -> atomic.convert()
        }
    }
}
