package com.gitlab.legoatoom.babycobolcompiler.ir

class IRAddSubExpr(
    private val left: IRArithExpr,
    private val right: IRArithExpr,
    private val addition: Boolean
) : IRArithExpr {
    override fun convert(): String {
        // If the operator is a +, use the plus()-function on BigIntegers.
        if (addition) {
            return "${left.convert()}.plus(${right.convert()})"
        }

        // If the operator is a -, use the minus()-function on BigIntegers.
        return "${left.convert()}.minus(${right.convert()})"
    }
}