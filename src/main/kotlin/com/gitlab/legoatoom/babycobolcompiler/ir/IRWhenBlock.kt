package com.gitlab.legoatoom.babycobolcompiler.ir

/**
 * @param conditions A list of IRAtomics, if empty it will become default/OTHER.
 * @param stats A list of IRStat, the statements to execute.
 */
class IRWhenBlock(
    private val conditions: List<IRAtomic>,
    private val stats: List<IRStat>
) : IRHelper {
    override fun convert(): String {
        if (stats.isEmpty()) return ""

        val convertedStats = stats.joinToString("\n") { it.convert().prependIndent() }.trimStart()

        if (conditions.isEmpty()) {
            return """
                |else -> {
                |    $convertedStats
                |}
            """.trimMargin()
        }

        // Map atomic correct, so that all types match (BigIntegers, Strings)
        var convertedConditions = conditions.map(IRAtomic::convert)

        convertedConditions = convertedConditions.map {
            if (it.startsWith('"') || it == "true" || it == "false") it
            else if (it.matches(Regex("^\\d.*$"))) "$it.toBigInteger()"
            else "$it.getValue()"
        }

        return """
            |${convertedConditions.joinToString(", ")} -> {
            |    $convertedStats
            |}
        """.trimMargin()
    }

}
