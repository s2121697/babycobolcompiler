package com.gitlab.legoatoom.babycobolcompiler.ir

class IRAccept(private val identifiers: List<IRIdentifier>) : IRStat() {

    override fun convert(): String {
        return identifiers.joinToString("\n") {
            "${it.convert()}.setValue(readln())"
        }
    }
}
