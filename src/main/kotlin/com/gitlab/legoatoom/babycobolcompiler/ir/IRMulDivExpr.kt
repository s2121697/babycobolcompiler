package com.gitlab.legoatoom.babycobolcompiler.ir

class IRMulDivExpr(
    private val left: IRArithExpr,
    private val right: IRArithExpr,
    private val multiplication: Boolean
) : IRArithExpr {
    override fun convert(): String {
        // If the operator is a *, use the times()-function on BigIntegers.
        if (multiplication) {
            return "${left.convert()}.times(${right.convert()})"
        }

        // If the operator is a /, use the div()-function on BigIntegers.
        return "${left.convert()}.div(${right.convert()})"
    }
}