package com.gitlab.legoatoom.babycobolcompiler.ir

import com.gitlab.legoatoom.babycobolcompiler.util.whenNotNull

class IRPerform(
    private val startPerform: IRIdentifier,
    private val endPerform: IRIdentifier?,
    private val times: IRAtomic?,
    private val paragraphs: List<String>
) : IRStat() {
    override fun convert(): String {
        var startOrStop = false

        val startConverted: String = startPerform.convert()
        val endConverted: String? = endPerform?.convert()

        val mapped = paragraphs.map { s -> IRIdentifier(s).convert() }
        val start: Int = mapped.indexOf(startConverted)
        var end: Int = start
        whenNotNull(endPerform) { end = mapped.indexOf(it.convert()) }

        val newScope = paragraphs.subList(start, end + 1)
            .joinToString(separator = "\",\"", prefix = "setOf(\"", postfix = "\")")

        val code = mapped.subList(start, end + 1).joinToString(separator = ";") { "$it()" }
        if (times != null){
            return """
                try{
                    scope.push($newScope)
                    var bi = ${times.convert()}.getValue(); while (bi > BigInteger.ZERO) {
                        $code
                        bi--
                    }
                    scope.
                } catch (ignore: GoToReturn){} finally { scope.pop() }
            """.trimIndent()
        }
        return "try{scope.push($newScope); $code}catch(ignore: GoToReturn){}finally{scope.pop()}"
    }
}
