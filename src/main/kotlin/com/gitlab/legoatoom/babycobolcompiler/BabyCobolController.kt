package com.gitlab.legoatoom.babycobolcompiler

import com.gitlab.legoatoom.babycobolcompiler.antlr.AntlrState
import com.gitlab.legoatoom.babycobolcompiler.argparser.ArgParserState
import com.gitlab.legoatoom.babycobolcompiler.argparser.Settings
import com.gitlab.legoatoom.babycobolcompiler.builder.BuilderState
import com.gitlab.legoatoom.babycobolcompiler.checker.CheckerState
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateResult
import com.gitlab.legoatoom.babycobolcompiler.exceptions.validate
import com.gitlab.legoatoom.babycobolcompiler.ir.IRProgram
import com.gitlab.legoatoom.babycobolcompiler.precompile.PreCompileState
import com.gitlab.legoatoom.babycobolcompiler.printer.PrinterState
import com.gitlab.legoatoom.babycobolcompiler.runner.RunnerState
import com.gitlab.legoatoom.babycobolcompiler.state.BabyCobolCompilerState
import com.gitlab.legoatoom.babycobolcompiler.writer.WriterState
import org.antlr.v4.runtime.tree.ParseTree
import kotlin.reflect.KMutableProperty0


class BabyCobolController {

    private lateinit var parseTree: ParseTree
    private lateinit var settings: Settings

    private infix fun <T, C> T.pipe(func: (T) -> C): C = func(this)
    private infix fun <T> T.apply(func: (T) -> Unit): T = func(this).run { this@apply }
    private infix fun <T, C> T.stage(func: (T) -> BabyCobolCompilerState<C>): StateResult<out C> = func(this).handle()
    private infix fun <T> T.store(func: KMutableProperty0<T>): T = apply(func::set)
    private infix fun <T, C> T.retrieve(func: KMutableProperty0<C>): Pair<T, C> = this to func.get()


    fun execute(args: Array<String>) {
        (args
                //Stage 1
                stage ::ArgParserState
                pipe ::validate
                store this::settings

                //Stage 2
                stage ::PreCompileState
                pipe ::validate

                //Stage 3
                stage ::AntlrState
                pipe ::validate
                store this::parseTree

                //Stage 4
                stage ::CheckerState
                pipe ::validate
                retrieve::parseTree

                //Stage 5
                stage ::BuilderState
                pipe ::validate
                pipe IRProgram::convert

                //Stage 6
                pipe this::handleFinalState
                )
    }

    private fun handleFinalState(codeString: String) {
        if (settings.doPrint || (!this.settings.doRun && settings.writeFile == null)) {
            PrinterState(codeString).handle() pipe ::validate
        }
        if (this.settings.doRun) {
            RunnerState(codeString).handle() pipe ::validate
        }
        if (settings.writeFile != null) {
            WriterState(settings.writeFile!!, codeString).handle() pipe ::validate
        }
    }
}

