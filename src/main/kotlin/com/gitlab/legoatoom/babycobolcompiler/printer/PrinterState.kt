package com.gitlab.legoatoom.babycobolcompiler.printer

import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateResult
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateSuccess
import com.gitlab.legoatoom.babycobolcompiler.state.BabyCobolCompilerState

class PrinterState(val codeString: String) : BabyCobolCompilerState<Unit>("Printer State") {
    override fun handle(): StateResult<out Unit> {
        println("PRINTING CONVERTED PROGRAM")
        println(codeString)
        println("FINISH")
        return StateSuccess(Unit)
    }
}
