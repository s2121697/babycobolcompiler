package com.gitlab.legoatoom.babycobolcompiler.exceptions


sealed class BabyCobolThrowable(message: String?) : Throwable(message) {

    class BabyCobolFailure(type: Pair<Int, String>, message: String?, state: String) : BabyCobolThrowable(
        buildString {
            append("FAILURE: A failure occurred during $state")
            appendLine()
            append("[0x${Integer.toHexString(type.first)}] ").append(type.second).append(":")
            message?.let {
                appendLine()
                append("      ")
                append(it)
            }
        })


    class BabyCobolErrors(val errors: List<FaultInstance>) : BabyCobolThrowable(buildString {
        if (errors.isEmpty()) return@buildString
        append("ERRORS:")
        errors.forEach {
            appendLine()
            append("[0x${Integer.toHexString(it.first.first)}] ").append(it.first.second).append(":")
            appendLine()
            append("      ")
            append(it.second)
            appendLine()
        }
    })

    class BabyCobolWarnings(warnings: List<FaultInstance>) : BabyCobolThrowable(buildString {
        if (warnings.isEmpty()) return@buildString
        append("Warnings:")
        for (it in warnings) {
            appendLine()
            append("[0x${Integer.toHexString(it.first.first)}] ").append(it.first.second).append(":")
            appendLine()
            append("      ")
            append(it.second)
            appendLine()
        }
    })


}
typealias BabyCobolFailure = BabyCobolThrowable.BabyCobolFailure
typealias BabyCobolErrors = BabyCobolThrowable.BabyCobolErrors
typealias BabyCobolWarnings = BabyCobolThrowable.BabyCobolWarnings

typealias FaultInstance = Pair<Pair<Int, String>, String>


