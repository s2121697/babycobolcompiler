package com.gitlab.legoatoom.babycobolcompiler.exceptions

import com.xenomachina.argparser.ShowHelpException
import kotlin.system.exitProcess

sealed class StateResult<Value> {
    data class Success<Value>(
        val value: Value,
        val warnings: BabyCobolWarnings? = null
    ) : StateResult<Value>()

    data class Exception<Value>(val exception: kotlin.Exception) : StateResult<Value>()

    data class Failure<Value>(val failure: BabyCobolFailure) : StateResult<Value>()

    data class Error<Value>(
        val error: BabyCobolErrors,
        val warnings: BabyCobolWarnings? = null
    ) : StateResult<Value>()
}
typealias StateSuccess<Value> = StateResult.Success<Value>
typealias StateException<Value> = StateResult.Exception<Value>
typealias StateFailure<Value> = StateResult.Failure<Value>
typealias StateError<Value> = StateResult.Error<Value>

@Throws(AssertionError::class)
fun <Value> validate(result: StateResult<out Value>): Value {
    when (result) {
        is StateFailure -> {
            System.err.println(result.failure.message)
            exitProcess(1)
        }
        is StateError -> {
            result.warnings?.let { println(it) }
            System.err.println(result.error.message)
            exitProcess(1)
        }
        is StateException -> {
            if (result.exception is ShowHelpException) {
                result.exception.printAndExit("BabyCobol", 80)
            }
            println(result.exception.printStackTrace())

            System.err.println(result.exception.message)
            exitProcess(1)
        }
        is StateSuccess -> {
            // Do nothing except print warnings
            result.warnings?.let { println(it) }
            return result.value
        }
    }
}
