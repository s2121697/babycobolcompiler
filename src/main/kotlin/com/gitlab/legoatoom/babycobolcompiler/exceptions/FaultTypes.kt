package com.gitlab.legoatoom.babycobolcompiler.exceptions


/* Fault types are Pairs of integers and Strings
* First number is type:
*  0 : suggestion
*  1 : warning
*  2 : error
*  3 : fatal
*
*/

// Common 0x_000
val IOException = 0x3001 to "IOError"

// ArgParserState 0x_100
val InvalidFileType = 0x3101 to "InvalidFileType"

// PreCompileState 0x_200
val UnknownStatusIndicator = 0x1201 to "UnknownStatusIndicatorWarning"


// AntlrState 0x_300
val SyntaxError = 0x2301 to "SyntaxError"
val AmbiguityError = 0x2302 to "AmbiguityError"
val AttemptingFullContext = 0x2303 to "AttemptingFullContextError"
val ContextSensitivity = 0x2304 to "ContextSensitivityError"

// CheckerState 0x_400
val InvalidLevelNumber = 0x2401 to "InvalidLevelNumberError"
val InvalidOccurs = 0x2402 to "InvalidOccursError"
val InvalidPicture = 0x2403 to "InvalidPictureError"
val InvalidDisplay = 0x2404 to "InvalidDisplayError"
val MissingGiving = 0x2405 to "MissingGivingError"
val UnresolvedReference = 0x2406 to "UnresolvedReferenceError"
val TypeError = 0x2407 to "TypeError"
val OutOfBound = 0x2408 to "OutOfBoundError"
val ConflictingParagraphName = 0x2409 to "ConflictingParagraphNameError"
val UndefinedParagraph = 0x2410 to "UndefinedParagraphError"
val InvalidLikeIdentifier = 0x2411 to "InvalidLikeIdentifierError"
val AmbiguousIdentifier = 0x2412 to "AmbiguousIdentifierError"

// BuilderState 0x_500

