package com.gitlab.legoatoom.babycobolcompiler.argparser

import com.gitlab.legoatoom.babycobolcompiler.exceptions.BabyCobolFailure
import com.gitlab.legoatoom.babycobolcompiler.exceptions.IOException
import com.gitlab.legoatoom.babycobolcompiler.exceptions.InvalidFileType
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import java.io.File

@Suppress("unused")
class Settings(parser: ArgParser) {

    val sourceFile: File by parser.positional(
        "SOURCE",
        help = "source filename"
    ) {
        File(this)
    }.addValidator {
        if (!sourceFile.exists()) {
            throw BabyCobolFailure(IOException, "File ${sourceFile.name} does not exist!", "ArgParserState")
        }
        if (!sourceFile.path.endsWith(".bcl")) {
            throw BabyCobolFailure(InvalidFileType, "Compiler requires a BabyCobol File (.bcl)", "ArgParserState")
        }
    }

    val doRun: Boolean by parser.flagging(
        "-R", "--run",
        help = "runs the converted code"
    ).default(false)

    val doPrint: Boolean by parser.flagging(
        "-P", "--print",
        help = "prints the converted code, will automatically be used if neither write nor run is called"
    ).default(false)

    val writeFile: File? by parser.storing(
        "-W", "--write",
        help = "write the converted kotlin script somewhere"
    ) {
        File(this)
    }.default(null)
}
