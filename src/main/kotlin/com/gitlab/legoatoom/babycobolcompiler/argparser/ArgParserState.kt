package com.gitlab.legoatoom.babycobolcompiler.argparser

import com.gitlab.legoatoom.babycobolcompiler.exceptions.*
import com.gitlab.legoatoom.babycobolcompiler.state.BabyCobolCompilerState
import com.xenomachina.argparser.ArgParser

class ArgParserState(private val args: Array<String>) :
    BabyCobolCompilerState<Settings>(name = "Argument Parser State") {

    override fun handle(): StateResult<out Settings> {
        return try {
            val settings = ArgParser(args).parseInto(::Settings)
            StateSuccess(settings)
        } catch (e: BabyCobolFailure) {
            StateFailure(e)
        } catch (e: Exception) {
            StateException(e)
        }
    }
}
