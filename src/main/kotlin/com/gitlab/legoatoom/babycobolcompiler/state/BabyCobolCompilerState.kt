package com.gitlab.legoatoom.babycobolcompiler.state

import com.gitlab.legoatoom.babycobolcompiler.exceptions.BabyCobolErrors
import com.gitlab.legoatoom.babycobolcompiler.exceptions.BabyCobolWarnings
import com.gitlab.legoatoom.babycobolcompiler.exceptions.FaultInstance
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateResult

/**
 * State Design Pattern
 */
abstract class BabyCobolCompilerState<ResultType>(
    val name: String
) {

    protected var warnings: MutableList<FaultInstance> = mutableListOf()
    protected var errors: MutableList<FaultInstance> = mutableListOf()

    protected fun getErrors() = if (errors.isEmpty()) null else BabyCobolErrors(errors)
    protected fun getWarnings() = if (warnings.isEmpty()) null else BabyCobolWarnings(warnings)

    abstract fun handle(): StateResult<out ResultType>
}
