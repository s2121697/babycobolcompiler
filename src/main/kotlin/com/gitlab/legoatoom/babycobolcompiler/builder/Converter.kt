package com.gitlab.legoatoom.babycobolcompiler.builder
//
//import antlr.BabyCobol
//import antlr.BabyCobolBaseVisitor
//import com.gitlab.legoatoom.babycobolcompiler.exceptions.ConverterException
//import org.antlr.v4.runtime.tree.ParseTree
//
//class Converter : BabyCobolBaseVisitor<String>() {
//
//    fun convert(tree : ParseTree): String {
//       return visit(tree)
//    }
//
//    override fun visitProgram(ctx: BabyCobol.ProgramContext): String {
//        return ctx.stat().joinToString("\n") { visitStat(it) }
//    }
//
//    override fun visitStat(ctx: BabyCobol.StatContext?): String {
//        return visitChildren(ctx)
//    }
//
//    // TODO: is correct functionality for subprograms/procedures?
//    override fun visitStop(ctx: BabyCobol.StopContext?): String {
//        return "kotlin.system.exitProcess(0)"
//    }
//
//    // TODO: check if variable is initialized or reassigned -> symbol table
//    override fun visitAdd(ctx: BabyCobol.AddContext?): String {
//        val leftOp = visitAtomic(ctx?.atomic(0))
//        val rightOp = visitAtomic(ctx?.atomic(1))
//
//        val literalStartRegex = Regex("[0-9.]")
//        if (literalStartRegex.matches(rightOp[0].toString())) { // Second atomic is literal
//            val id = visitIdentifier(ctx?.identifier())
//            return "var $id = $leftOp + $rightOp"
//        }
//
//        return "$rightOp += $leftOp"
//    }
//
//    // TODO: check if variable is initialized or reassigned -> symbol table
//    override fun visitSubtract(ctx: BabyCobol.SubtractContext?): String {
//        val leftOp = visitAtomic(ctx?.atomic(0))
//        val rightOp = visitAtomic(ctx?.atomic(1))
//
//        val literalStartRegex = Regex("[0-9.]")
//        if (literalStartRegex.matches(rightOp[0].toString())) { // Second atomic is literal
//            val id = visitIdentifier(ctx?.identifier())
//            return "var $id = $rightOp - $leftOp"
//        }
//
//        return "$rightOp -= $leftOp"
//    }
//
//
//    override fun visitDisplay(ctx: BabyCobol.DisplayContext?): String {
//        if (ctx?.WITH() != null && ctx.NO() != null && ctx.ADVANCING() != null) { // WITH NO ADVANCING all present
//            // Visit just the atomic, not the other children
//            return "print(${visitAtomic(ctx.atomic())})"
//        } else if (ctx?.WITH() != null || ctx?.NO() != null || ctx?.ADVANCING() != null) { // Some of WITH NO ADVANCING present
//            throw ConverterException("expected 'WITH NO ADVANCING'!")
//        }
//
//        // WITH NO ADVANCING all not present
//        return "println(${visitAtomic(ctx?.atomic())})"
//    }
//
//    override fun visitAtomic(ctx: BabyCobol.AtomicContext?): String {
//        return visitChildren(ctx)
//    }
//
//    override fun visitLiteral(ctx: BabyCobol.LiteralContext?): String {
//        val childToken = ctx?.getStart() // Get first child of literal
//        if (childToken?.text == null) {
//            throw ConverterException("expected an literal!")
//        }
//
//        return childToken.text
//    }
//
//    override fun visitIdentifier(ctx: BabyCobol.IdentifierContext?): String {
//        val childToken = ctx?.getStart() // Get first child of literal
//        if (childToken?.text == null) {
//            throw ConverterException("expected an identifier!")
//        }
//
//        return childToken.text.lowercase()
//    }
//}
