package com.gitlab.legoatoom.babycobolcompiler.builder

import com.gitlab.legoatoom.babycobolcompiler.checker.SymbolTable
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateResult
import com.gitlab.legoatoom.babycobolcompiler.ir.IRProgram
import com.gitlab.legoatoom.babycobolcompiler.state.BabyCobolCompilerState
import org.antlr.v4.runtime.tree.ParseTree

class BuilderState(pair: Pair<SymbolTable, ParseTree>) :
    BabyCobolCompilerState<IRProgram>("BuilderState") {

    private val parseTree: ParseTree = pair.second
    private val builder: Convertor = Convertor(pair.first)


    override fun handle(): StateResult<out IRProgram> {
        return builder.build(parseTree)
    }
}
