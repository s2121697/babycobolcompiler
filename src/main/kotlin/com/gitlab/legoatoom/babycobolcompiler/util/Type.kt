package com.gitlab.legoatoom.babycobolcompiler.util

enum class Type {
    BOOL, INT, DECIMAL, STRING
}