package com.gitlab.legoatoom.babycobolcompiler.util

import antlr.BabyCobol
import org.antlr.v4.runtime.tree.TerminalNode

/**
 * A helper function to get the value of an index of an identifier. The index is represented as a [String] with
 * opening and closing brackets. These are removed and the resulting [String] is converted to an [Int].
 *
 * @param ctx The top-level identifier node.
 * @returns The value of the index as an [Int].
 */
fun getIndexValue(ctx: BabyCobol.IdentifierContext): Int {
    // Get the text and remove the brackets from it and filter out the whitespace.
    val intText = ctx.INDEX().text.drop(1).dropLast(1).filter { !it.isWhitespace() }
    return intText.toInt()
}

/**
 * A helper function to get the id as a [List] of [String]'s from the top-level identifier node. It uses
 * breadth-first search on the root node (top-level identifier). It works because in the grammar it is defined
 * that the identifier optionally has a (OF identifier) clause to the right of it.
 *
 * @param identifier The top-level identifier node.
 * @returns The id of the identifier as a [List] of [String]'s.
 */
fun getIdentifierId(identifier: BabyCobol.IdentifierContext): List<String> {
    val left = identifier.identifier(0)
    val right = identifier.identifier(1)
    val res: MutableList<String> = emptyList<String>().toMutableList()

    if (left != null) {
        res += getIdentifierId(left)
    }

    if (right != null) {
        res += getIdentifierId(right)
    }

    // Get the text of the identifier if its child is a terminal node, except if it is an INDEX.
    if (identifier.getChild(0) is TerminalNode && identifier.INDEX() == null) {
        res += identifier.text
    }

    return res
}

/**
 * A helper function to get the name from the identifier as a [String]. It uses the getIdentifierName function to get
 * the id of the identifier and then transforms it to a name.
 *
 * @param identifier The top-level identifier node.
 * @returns The name of the identifier as a [String].
 */
fun getIdentifierName(identifier: BabyCobol.IdentifierContext): String {
    val id = getIdentifierId(identifier)

    return "_" + id.joinToString("of").lowercase()
}

/**
 * A helper function to get the name from the identifier as a [String] from as id, e.g. a [List] of [String]s.
 *
 * @param id The id of the identifier as a [List] of [String]s.
 * @returns The name of the identifier as a [String].
 */
fun getIdentifierNameFromId(id: List<String>): String {
    return id.joinToString("of").lowercase()
}

/**
 * A helper function to get the index of the left-subtree from the top-level identifier node. It just takes the
 * first index it encounters. If there is no index present, it returns null.
 *
 * @param identifier The top-level identifier node.
 * @returns The index of the identifier as an [Int].
 */
fun getLeftSubtreeIndex(identifier: BabyCobol.IdentifierContext): Int? {
    val left = identifier.identifier(0)

    if (identifier.INDEX() != null) {
        return getIndexValue(identifier)
    }

    if (left != null) {
        return getLeftSubtreeIndex(left)
    }

    return null
}

/**
 * A helper function to get the sequence of indices from the top-level identifier node. It will search the tree
 * from right to left, so the list starts with the index from the highest parent.
 *
 * @param identifier The top-level identifier node.
 * @returns A list of indices from the identifier as a [List] of [Int]s.
 */
fun getIndexSequence(identifier: BabyCobol.IdentifierContext): List<Int> {
    val left = identifier.identifier(0)
    val right = identifier.identifier(1)
    val res = emptyList<Int>().toMutableList()

    if (right != null) {
        res += getIndexSequence(right)
    }

    if (left != null) {
        res += getIndexSequence(left)
    }

    if (identifier.INDEX() != null) {
        // Minus 1 because indices in Kotlin are 0-based while BabyCobol is 1 -based
        res += (getIndexValue(identifier) - 1)
    }

    return res
}

/**
 * A helper function to get the sequence of indices as a String from the top-level identifier node. It uses the
 * getIndexSequence function and then transforms that [List] to a [String].
 *
 * @param identifier The top-level identifier node.
 * @returns A [String] representing the indices of the identifier.
 */
fun getIndexString(identifier: BabyCobol.IdentifierContext): String? {
    val indexSequence = getIndexSequence(identifier)

    if (indexSequence.isNotEmpty()) {
        return "[" + indexSequence.joinToString("][") + "]"
    }

    return null
}

