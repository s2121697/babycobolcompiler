package com.gitlab.legoatoom.babycobolcompiler.util

/**
 * Class with information about an identifier (data entry).
 *
 * Level number and line number are used to derive the parents of the identifier. Occurs is used to determine the
 * length of the array for the identifier (1 is no array, > 1 is an array). The other attributes are used to
 * validate and format the value of the identifier.
 */
data class IdentifierInfo(
    val lvlNum: Int,
    val line: Int,
    var regex: Regex? = null,
    var padSymbol: Char = ' ',
    var len: Int = 0,
    var returnType: String = "String",
    var occurs: Int = 0
)
