package com.gitlab.legoatoom.babycobolcompiler.util

class OrderedHashMap<K, V>() : LinkedHashMap<K, V>() {
    val elements: MutableList<K> = mutableListOf()

    fun getKey(i: Int): K {
        return elements[i]
    }

    fun getElement(i: Int): V? {
        return get(elements[i])
    }

    override fun put(key: K, value: V): V? {
        elements.add(key)
        return super.put(key, value)
    }

    override fun putAll(from: Map<out K, V>) {
        for ((key, value) in from) {
            put(key, value)
        }
    }

    override fun remove(key: K): V? {
        elements.remove(key)
        return super.remove(key)
    }

    override fun clear() {
        elements.clear()
        super.clear()
    }
}
