// Scope Stack
val scope : java.util.Stack<Set<String>> = java.util.Stack()

// Declare Variables

// Begin Code
try {




// Paragraphs Call
    _a();_b();_c();_d();_e();_f()
} catch (ignore: ExitScript) {}

fun _a(){
    print("A")
    try{scope.push(setOf("B")); _b()}catch(ignore: GoToReturn){}finally{scope.pop()}
    try{scope.push(setOf("B","C")); _b();_c()}catch(ignore: GoToReturn){}finally{scope.pop()}
    try{scope.push(setOf("D","E")); _d();_e()}catch(ignore: GoToReturn){}finally{scope.pop()}
    try{scope.push(setOf("C","D")); _c();_d()}catch(ignore: GoToReturn){}finally{scope.pop()}
    print("A")
}
fun _b(){
    print("B")
}
fun _c(){
    print("C")
}
fun _d(){
    print("D")
    _e();if (scope.peek().contains("E")) { throw GoToReturn()};_f();if (scope.peek().contains("F")) { throw GoToReturn()};throw ExitScript()
    print("D")
}
fun _e(){
    print("E")
}
fun _f(){
    print("F")
    throw ExitScript()
}

// Boilerplate
open class Picture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) {
    private var value: String = padSymbol.toString().repeat(len)

    open fun getValue(): Any { return this.value }

    override fun toString(): String { return this.value }

    fun setValue(newVal: String) {
        // Check length of newVal. Pad to left with spaces / 0's or cut left most part of string.
        var updatedVal: String = newVal
        if (newVal.length < len) {
            updatedVal = newVal.padStart(len, padSymbol)
        } else if (newVal.length > len) {
            updatedVal = newVal.takeLast(len)
        }

        // Check if value correspond with regex
        if (regex.matches(updatedVal)) this.value = updatedVal
        else {
            val errMessage = "Cannot assign " + updatedVal + " to data entry " + this.name
            throw Exception(errMessage)
        }
    }
}

class IntPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
    override fun getValue(): java.math.BigInteger {
        return super.getValue().toString().toBigInteger()
    }
}

class StringPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
    override fun getValue(): String {
        return super.getValue().toString()
    }
}

class GoToReturn : Throwable()
class ExitScript : Throwable()
