package com.gitlab.legoatoom.babycobolcompiler.util

/**
 * Class with information about a paragraph (data entry).
 *
 * Data is mostly used for error/warning printing.
 */
data class ParagraphInfo(
    val name: String,
    var callers: MutableSet<Pair<Int, Int>>,
    var line: Int,
    var column: Int,
    var called: Boolean,
    var defined: Boolean
)
