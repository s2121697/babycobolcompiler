package com.gitlab.legoatoom.babycobolcompiler.util

/**
 * Class for Picture regular expressions.
 *
 * Until now (level 3) a Picture in BabyCobol can only have a pattern consisting solely of 9's or solely of X's. The
 * class has some useful methods checking Picture representation string and getting certain attributes givin a
 * representation string.
 */
private val nines = Regex("^[9]+$")
private val exes = Regex("^[X]+$")

private fun getNinesPattern(n: Int): Regex {
    val str = "[0-9]{$n}"
    return Regex("^$str$")
}

private fun getExesPattern(n: Int): Regex {
    val str = ".{$n}"
    return Regex("^$str$")
}

/**
 * Checks if the representation string is valid.
 *
 * @param reprString The representation string of the picture.
 * @returns true if valid, false otherwise.
 */
fun isValidRepresentation(reprString: String): Boolean {
    if (nines.matches(reprString)) return true
    if (exes.matches(reprString)) return true
    return false
}

/**
 * Get the matching regex for a representation string, taking length into account.
 *
 * @param reprString The representation string of the picture.
 * @returns The matching [Regex] or null if no such [Regex] exists.
 */
fun getMatchingRegex(reprString: String): Regex? {
    val n = reprString.length
    if (nines.matches(reprString)) return getNinesPattern(n)
    if (exes.matches(reprString)) return getExesPattern(n)
    return null
}

/**
 * Get the padding symbol for a representation string.
 *
 * @param reprString The representation string of the picture.
 * @returns The padding symbol [Char] used for formatting.
 */
fun getPadSymbol(reprString: String): Char? {
    if (nines.matches(reprString)) return '0'
    if (exes.matches(reprString)) return ' '
    return null
}

/**
 * Get the return type for a representation string.
 *
 * @param reprString The representation string of the picture.
 * @returns The return type as [String] used for determining the Picture subclass for an identifier.
 */
fun getReturnType(reprString: String): String {
    if (nines.matches(reprString)) return "Int"
    return "String"
}
