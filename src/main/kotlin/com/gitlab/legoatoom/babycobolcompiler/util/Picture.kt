package com.gitlab.legoatoom.babycobolcompiler.util

import java.math.BigInteger

// TODO: maybe compile then import this in the generated program.
open class Picture(
    private val regex: Regex,
    private val len: Int,
    private val padSymbol: Char,
    private val name: String
) {
    private var value: String = padSymbol.toString().repeat(len)

    open fun getValue(): Any {
        return this.value
    }

    override fun toString(): String {
        return this.value
    }

    fun setValue(newVal: String) {
        // Check length of newVal. Pad to left with spaces / 0's or cut left most part of string.
        var updatedVal: String = newVal
        if (newVal.length < len) {
            updatedVal = newVal.padStart(len, padSymbol)
        } else if (newVal.length > len) {
            updatedVal = newVal.takeLast(len)
        }

        // Check if value correspond with regex
        if (regex.matches(updatedVal)) this.value = updatedVal
        else {
            val errMessage = "Cannot assign " + updatedVal + " to data entry " + this.name
            throw Exception(errMessage)
        }
    }
}

class IntPicture(
    private val regex: Regex,
    private val len: Int,
    private val padSymbol: Char,
    private val name: String
) : Picture(regex, len, padSymbol, name) {
    override fun getValue(): BigInteger {
        return super.getValue().toString().toBigInteger()
    }
}

class StringPicture(
    private val regex: Regex,
    private val len: Int,
    private val padSymbol: Char,
    private val name: String
) : Picture(regex, len, padSymbol, name) {
    override fun getValue(): String {
        return super.getValue().toString()
    }
}

var _test = IntPicture(Regex("^[0-9]{3}$"), 3, '0', "_test")

fun main() {
    _test.setValue((8.toBigInteger().plus(_test.getValue())).toString())
    var bi = _test.getValue(); while (bi > BigInteger.ZERO) {

        bi--
    }

}
