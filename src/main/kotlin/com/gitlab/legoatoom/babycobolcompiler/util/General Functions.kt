package com.gitlab.legoatoom.babycobolcompiler.util

inline fun <T : Any, R> whenNotNull(input: T?, callback: (T) -> R): R? {
    return input?.let(callback)
}
