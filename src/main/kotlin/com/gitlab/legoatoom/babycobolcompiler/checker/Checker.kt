package com.gitlab.legoatoom.babycobolcompiler.checker

import antlr.BabyCobol
import antlr.BabyCobolBaseListener
import com.gitlab.legoatoom.babycobolcompiler.exceptions.*
import com.gitlab.legoatoom.babycobolcompiler.util.*
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.tree.ParseTree
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.tree.TerminalNode

class Checker(private val errors: MutableList<FaultInstance>, private val warnings: MutableList<FaultInstance>) :
    BabyCobolBaseListener() {

    private val symbolTable: SymbolTable = SymbolTable()

    private fun Token.addError(error: Pair<Int, String>, message: (Token) -> String) {
        errors.add(error to (buildString {
            append(message.invoke(this@addError))
            append(System.lineSeparator())
            append("at ${this@addError.line}:${this@addError.charPositionInLine}")
        }))
    }


    // Walk the parse tree and get all errors and warnings. If there are errors, throw an exception.
    fun check(tree: ParseTree): StateResult<SymbolTable> {
        try {
            ParseTreeWalker().walk(this, tree)
            postWalkCheck()
            if (errors.isNotEmpty()) {
                return StateError(
                    BabyCobolErrors(errors),
                    if (warnings.isEmpty()) null else BabyCobolWarnings(warnings)
                )
            }
            return StateSuccess(symbolTable, if (warnings.isEmpty()) null else BabyCobolWarnings(warnings))
        } catch (failure: BabyCobolFailure) {
            return StateFailure(failure)
        } catch (exception: Exception) {
            return StateException(exception)
        }
    }

    /**
     * After walking, there are post checks.
     */
    private fun postWalkCheck() {
        for (paragraph: ParagraphInfo in symbolTable.getUndefinedParagraphs()) {
            errors.add(UndefinedParagraph to buildString {
                append("Undefined paragraph '${paragraph.name}'")
                append(System.lineSeparator())
                append("at: ")
                append(paragraph.callers.joinToString(separator = ",", prefix = "[", postfix = "]") { (row, column) ->
                    "${row}:${column}"
                })
            })
        }
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation does nothing.
     */
    override fun enterDataDivision(ctx: BabyCobol.DataDivisionContext) {
        // Get the top-level level number.
        val topLvlNum = ctx.dataEntry(0).INT_LIT().text

        // Iterate over every data-entry, in order!
        ctx.dataEntry().forEachIndexed { index, entry ->
            // Check if the level numbers are not lower than the top level number.
            val current: String = entry.INT_LIT().text
            if (current.toInt() < topLvlNum.toInt()) {
                entry.INT_LIT().symbol.addError(InvalidLevelNumber) {
                    "'$current' cannot be less than '$topLvlNum'"
                }
            }

            // Set the parents of the current data entry and derive the id from it.
            val parents = getParents(current.toInt(), index)
            val id: List<String> = listOf(entry.identifier().text) + parents

            // Initialize the identifier with basic identifier info.
            val info = IdentifierInfo(current.toInt(), index)
            setIdentifierInfo(id, info)

            // Extend the identifier occurs info for the current data entry, if a OCCURS-clause is present.
            if (entry.occursClause() != null) {
                // Check if n in OCCURS-clause is bigger or equal to 1
                if (entry.occursClause().n.text.toInt() < 1) {
                    setIdentifierOccurs(id, entry.occursClause().n.text)
                    ctx.getStart().addError(InvalidOccurs) {
                        "OCCURS-clause must have a positive integer greater than 0, but is ${entry.occursClause().n.text.toInt()} for field '${
                            id.joinToString(" OF ")
                        }'"
                    }
                }

                setIdentifierOccurs(id, entry.occursClause().n.text)

                // Check if one of the parents has a Picture-clause, this is illegal
                var parentId = parents
                while (parentId.isNotEmpty()) {
                    if (getBabyCobolType(parentId) != null) {
                        ctx.getStart().addError(InvalidOccurs) {
                            "Parent '${parentId.joinToString(" OF ")}' has a PICTURE-clause so nested fields cannot have an OCCURS-clause for field '${
                                id.joinToString(" OF ")
                            }'"
                        }
                    }
                    parentId = parentId.drop(1)
                }
            }

            // Extend the identifier info for the current data entry, if a PICTURE-clause is present.
            if (entry.pictureClause() != null) {
                val reprString = entry.pictureClause().representation().getStart().text
                val matchingRegex: Regex? = getMatchingRegex(reprString)
                val padSymbol: Char? = getPadSymbol(reprString)
                val returnType: String = getReturnType(reprString)
                if (matchingRegex != null && padSymbol != null) {
                    setIdentifierTypeLenSymbol(id, matchingRegex, reprString.length, padSymbol, returnType)
                }
            }

            // Extend the identifier info for the current data entry, if a LIKE-clause is present.
            if (entry.likeClause() != null) {
                // Check if the referenced identifier is valid. It can not have any index.
                checkLikeIdentifier(entry.likeClause().identifier())

                val likeIdentifier = entry.likeClause().identifier().text.uppercase()
                // Has to be altered for sufficient qualification
                val likeId = likeIdentifier.split("OF")

                // Get Identifier info of the identifier whose pattern we have to match.
                val identifierInfo = getIdentifierInfo(likeId)

                if (identifierInfo != null) {
                    // Check if it is record or field.
                    if (isRecordIdentifier(likeId)) {
                        // If record -> copy all its children and change the name.
                        val children = getChildren(likeId, identifierInfo.lvlNum)
                        for (childId in children) {
                            // Get info of the child
                            val childInfo = getIdentifierInfo(childId)
                            if (childInfo != null) {
                                // Initialize a new identifier with basic identifier info.
                                val newInfo = IdentifierInfo(childInfo.lvlNum, childInfo.line)
                                val newId = childId.toMutableList()
                                val parentIndex = newId.indexOf(likeId[0])

                                // Change id to use the current identifier instead of the like-identifier.
                                newId[parentIndex] = entry.identifier().text

                                // Set the id, basic info (lvlNUm, line) and occurs.
                                setIdentifierInfo(newId, newInfo)
                                setIdentifierOccurs(newId, childInfo.occurs.toString())

                                // Set the regex, len, padSymbol and returntype.
                                if (childInfo.regex != null) {
                                    setIdentifierTypeLenSymbol(newId,
                                        childInfo.regex!!, childInfo.len, childInfo.padSymbol, childInfo.returnType)
                                }
                            }
                        }
                    } else {
                        // If field -> just copy it without the occurs
                        setIdentifierTypeLenSymbol(id, identifierInfo.regex!!, identifierInfo.len, identifierInfo.padSymbol, identifierInfo.returnType)
                    }
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation does nothing.
     */
    override fun exitDataEntry(ctx: BabyCobol.DataEntryContext) {
        // Check length of level number (must be exactly two).
        if (ctx.INT_LIT().text.length != 2) {
            ctx.INT_LIT().symbol.addError(InvalidLevelNumber) {
                "Invalid level number: ${ctx.INT_LIT().text}, must be exactly two digits"
            }
        }
    }

    /**
     * Enter a paragraph, keep track of the defined paragraphs.
     */
    override fun enterParagraph(ctx: BabyCobol.ParagraphContext) {
        val name = ctx.identifier().text
        val line = ctx.identifier().getStart().line
        val column = ctx.identifier().getStart().charPositionInLine
        symbolTable.getParagraph(name)?.let { paragraphInfo ->
            if (!paragraphInfo.defined) return@let
            errors.add(ConflictingParagraphName to buildString {
                append("Conflicting '$name' paragraph definition")
                append(System.lineSeparator())
                append("at ${paragraphInfo.line}:${paragraphInfo.column}")
                append("and ${line}:${column}")
            })
            return
        }

        symbolTable.setParagraphDefined(ParagraphInfo(
            ctx.identifier().text,
            mutableSetOf(),
            ctx.identifier().getStart().line,
            ctx.identifier().getStart().charPositionInLine,
            called = false,
            defined = true
        ))
    }

    override fun enterGoToStat(ctx: BabyCobol.GoToStatContext) {
        symbolTable.setParagraphCalled(ctx.identifier().text,
            ctx.identifier().getStart().line to ctx.identifier().getStart().charPositionInLine)
    }

    override fun enterPerformStat(ctx: BabyCobol.PerformStatContext) {
        symbolTable.setParagraphCalled(ctx.startProc.text,
            ctx.startProc.getStart().line to ctx.startProc.getStart().charPositionInLine)
        whenNotNull(ctx.endProc) {
            symbolTable.setParagraphCalled(it.text,
                it.getStart().line to it.getStart().charPositionInLine)
        }
        whenNotNull(ctx.atomic()){
            setType(ctx.atomic(), Type.INT)
        }
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation does nothing.
     */
    override fun exitPictureClause(ctx: BabyCobol.PictureClauseContext) {
        if (ctx.PICTURE() == null) return

        // Check if representation string consists solely of 9's or solely of X's (required by level 3 feature)
        val reprString = ctx.representation().getStart().text

        // Check if the representation string is valid.
        if (!isValidRepresentation(reprString)) {
            ctx.representation().getStart().addError(InvalidPicture) {
                "Invalid representation string: $reprString, must consists solely of 9's or X's"
            }
        }
    }
    /**
     * On the exit of an ADD token in the parse tree, check if it is a valid statement.
     *
     */
    override fun exitAddStat(ctx: BabyCobol.AddStatContext) {
        checkArithmeticStat(ctx.atomic(0), ctx.atomic(1), ctx.GIVING(), ctx.identifier())
    }

    /**
     * On the exit of an SUBTRACT token in the parse tree, check if it is a valid statement.
     */
    override fun exitSubtractStat(ctx: BabyCobol.SubtractStatContext) {
        checkArithmeticStat(ctx.atomic(0), ctx.atomic(1), ctx.GIVING(), ctx.identifier())
    }

    /**
     * On the exit of an SUBTRACT token in the parse tree, check if it is a valid statement.
     */
    override fun exitMultiplyStat(ctx: BabyCobol.MultiplyStatContext) {
        checkArithmeticStat(ctx.atomic(0), ctx.atomic(1), ctx.GIVING(), ctx.identifier())
    }

    /**
     * On the exit of an SUBTRACT token in the parse tree, check if it is a valid statement.
     */
    override fun exitDivideStat(ctx: BabyCobol.DivideStatContext) {
        checkArithmeticStat(ctx.atomic(0), ctx.atomic(1), ctx.GIVING(), ctx.identifier(0), divide = true, remainder = ctx.REMAINDER(), remainderIdentifier = ctx.identifier(1))
    }

    /**
     * On the exit of an PERFORM we check if the times is a valid statement.
     */
    override fun exitPerformStat(ctx: BabyCobol.PerformStatContext) {
        whenNotNull(ctx.atomic()) { checkType(it, Type.INT)}
    }

    /**
     * A helper function to check if the arithmetic statement (add, subtract, multiply, divide) is correct. It adds
     * errors if it finds them. The function checks if the GIVING-clause is present when it must be present, if the
     * identifier in the GIVING-clause is numerical and if the left- and right-operand are numerical and of the same
     * type (int vs decimal).
     *
     * @param leftAtomic The [BabyCobol.AtomicContext] of the left operand.
     * @param rightAtomic The [BabyCobol.AtomicContext] of the right operand.
     * @param giving The [TerminalNode] of GIVING if it is present.
     * @param identifier The [BabyCobol.IdentifierContext] of the identifier in the GIVING-clause.
     */
    private fun checkArithmeticStat(
        leftAtomic: BabyCobol.AtomicContext,
        rightAtomic: BabyCobol.AtomicContext,
        giving: TerminalNode?,
        identifier: BabyCobol.IdentifierContext?,
        remainderIdentifier: BabyCobol.IdentifierContext? = null,
        remainder: TerminalNode? = null,
        divide: Boolean = false
    ) {
        // If the second atomic is a literal and no GIVING-clause is present, it is invalid.
        // For divide: if the first atomic is a literal and no GIVING-clause and no REMAINDER-clause is present, it is invalid.
        if (divide && leftAtomic is BabyCobol.LiteralAtomicContext && giving == null && remainder == null) {
            rightAtomic.start.addError(MissingGiving) { "Missing Giving" }
        } else if (!divide && rightAtomic is BabyCobol.LiteralAtomicContext && giving == null) {
            rightAtomic.start.addError(MissingGiving) { "Missing Giving" }
        }

        // If GIVING-clause is present, check if the identifier is has a numerical type.
        if ((giving != null && identifier != null) || (remainder != null && identifier != null)) {
            checkNumericalType(identifier)
        }

        // Check if both atomics are numerical and have equal types.
        checkNumericalType(leftAtomic)
        checkNumericalType(rightAtomic)
        checkEqualType(leftAtomic, rightAtomic, false)

        if (remainderIdentifier != null) {
            checkNumericalType(remainderIdentifier)
        }
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation does nothing.
     */
    override fun exitLiteralAtomic(ctx: BabyCobol.LiteralAtomicContext) {
        val inferredType = getType(ctx.literal())
        if (inferredType != null) setType(ctx, inferredType)
    }

    /**
     * {@inheritDoc}
     *
     *
     * The default implementation does nothing.
     */
    override fun exitIdAtomic(ctx: BabyCobol.IdAtomicContext) {
        val childType = getType(ctx.identifier())
        if (childType != null) setType(ctx, childType)
    }

    /**
     * Set type of the IntLit node in the symbol table.
     */
    override fun exitIntLit(ctx: BabyCobol.IntLitContext) {
        setType(ctx, Type.INT)
    }

    /**
     * Set type of the NumLit node in the symbol table.
     */
    override fun exitNumLit(ctx: BabyCobol.NumLitContext) {
        setType(ctx, Type.DECIMAL)
    }


    /**
     * {@inheritDoc}
     *
     *
     * The default implementation does nothing.
     */
    override fun exitIdentifier(ctx: BabyCobol.IdentifierContext) {
        // Check if this identifier is nested or not. If the parent is an identifier, don't do anything!
        // The identifier is handled by the top-level identifier.
        // If the parent is a paragraph or data entry, then we also do not have to check anything.
        val parent = ctx.getParent()
        if (parent is BabyCobol.IdentifierContext || parent is BabyCobol.ParagraphContext ||
            parent is BabyCobol.DataEntryContext || parent is BabyCobol.LikeClauseContext) return

        var currentId: BabyCobol.IdentifierContext? = ctx

        // Check if the current id is unambiguous.
        if (currentId != null) {
            val id = getIdentifierId(currentId)
            if (!isUnambiguousIdentifier(id)) {
                ctx.getStart().addError(AmbiguousIdentifier) {
                    "Identifier is ambiguous: '${id.joinToString(" OF ")}'"
                }
            }
        }

        // Check if the current identifier and all its parent are valid.
        while (currentId != null) {
            // Check if current identifier is valid.
            checkIdentifier(currentId)

            // Get the right subtree of identifiers.
            currentId = currentId.identifier(1)
        }
    }


    /**
     * A helper function to check if the identifier is valid, e.g. it is defined and properly indexed. It adds an
     * error if it finds that the identifier is invalid.
     *
     * @param ctx The context of the identifier to be checked.
     */
    private fun checkIdentifier(ctx: BabyCobol.IdentifierContext) {
        // Get the id as a list of strings.
        val id = getFullName(getIdentifierId(ctx))
        val index = getLeftSubtreeIndex(ctx)

        val inDisplay = ctx.getParent().getParent() is BabyCobol.DispOptionsContext

        // Check if the identifier:
        //     is valid, e.g. defined in data division.
        //     is an array but has no index.
        //     is not an array but has an index
        //     has an index and the index is valid for this identifier (not out of bounds)
        //
        // If all checks pass, set the type of the node by getting the BabyCobol type from the identifier.
        if (id != null && isValidParagraph(id)) {
            return // Good behaviour
        } else if (id == null && inDisplay) { // Unresolved keyword gets converted to String in Display
            setType(ctx, Type.STRING)
        }else if (id == null || !isValidIdentifier(id)) {
            ctx.getStart().addError(UnresolvedReference) {
                "Unresolved reference: '${id?.joinToString(" OF ")}'"
            }
        } else if (index == null && isArrayIdentifier(id)) {
            ctx.getStart().addError(UnresolvedReference) {
                "Array needs an index: '${id.joinToString(" OF ")}'"
            }
        } else if (index != null && !isArrayIdentifier(id)) {
            ctx.getStart().addError(TypeError) {
                "Illegal index, variable is not an array: '${id.joinToString(" OF ")}'"
            }
        } else if (index != null && !isValidIndexedIdentifier(id, index)) {
            ctx.getStart().addError(OutOfBound) {
                "Index out of bounds for: '${id.joinToString(" OF ")}'"
            }
        } else {
            val type: Type? = getBabyCobolType(id)
            if (type != null) setType(ctx, type)
        }

    }

    /**
     * A helper function to check if the identifier in the LIKE-clause is valid, e.g. it is defined and does not
     * hav an index. It adds error if it finds that the identifier is invalid.
     *
     * @param ctx The context of the identifier to be checked.
     */
    private fun checkLikeIdentifier(ctx: BabyCobol.IdentifierContext) {
        // Get the id as a list of strings.
        val id = getIdentifierId(ctx)
        val index = getLeftSubtreeIndex(ctx)

        // Check if the identifier:
        //     is valid, e.g. defined in data division.
        //     does not have an index
        //
        // If all checks pass, set the type of the node by getting the BabyCobol type from the identifier.
        if (!isValidIdentifier(id)) {
            ctx.getStart().addError(UnresolvedReference) {
                "Unresolved reference: '${id.joinToString(" OF ")}'"
            }
        } else if (index != null) {
            ctx.getStart().addError(InvalidLikeIdentifier) {
                "An identifier in the LIKE-clause cannot have an index: '${id.joinToString(" OF ")}'"
            }
        } else {
            val type: Type? = getBabyCobolType(id)
            if (type != null) setType(ctx, type)
        }
    }

    /**
     * Helpful method to set the type of a node in the [symbolTable].
     *
     * @param node The node that the type will be set of.
     * @param type The type you want to set.
     */
    private fun setType(node: ParseTree, type: Type) {
        this.symbolTable.setType(node, type)
    }

    /**
     * Helpful method to get the type of a node in the [symbolTable].
     *
     * @param node The node you want to get the type of.
     * @return The [Type] of the node.
     */
    private fun getType(node: ParseTree): Type? {
        return this.symbolTable.getType(node)
    }

    /**
     * Helpful method to set the regex that specifies the type of an identifier in the [symbolTable].
     *
     * @param id The name of the identifier.
     * @param type The regex type you want to set.
     * @param len The length of the regex.
     * @param symbol The symbol to be pad to the front of the value.
     */
    private fun setIdentifierTypeLenSymbol(id: List<String>, type: Regex, len: Int, symbol: Char, returnType: String) {
        this.symbolTable.setIdentifierRegex(id, type)
        this.symbolTable.setIdentifierLen(id, len)
        this.symbolTable.setIdentifierPadSymbol(id, symbol)
        this.symbolTable.setIdentifierReturnType(id, returnType)
    }

    /**
     * Helpful method to set the regex that specifies the type of an identifier in the [symbolTable].
     *
     * @param id The name of the identifier.
     * @returns [IdentifierInfo] object of the id.
     */
    private fun getIdentifierInfo(id: List<String>): IdentifierInfo? {
        return this.symbolTable.getIdentifierInfo(id)
    }

    /**
     * Helpful method to set the occurs attribute of an identifier in the [symbolTable].
     *
     * @param id The name of the identifier.
     * @param n The occurs value to be set.
     */
    private fun setIdentifierOccurs(id: List<String>, n: String) {
        this.symbolTable.setIdentifierOccurs(id, n.toInt())
    }

    /**
     * Helpful method to set the identifier information of an identifier in the [symbolTable].
     *
     * @param id The name of the identifier.
     * @param info The identifier information object of the identifier.
     */
    private fun setIdentifierInfo(id: List<String>, info: IdentifierInfo) {
        this.symbolTable.setIdentifier(id, info)
    }

    /**
     * Helpful method to get the parents of an identifier.
     *
     * @param lvlNum The level number of the identifier.
     * @param line The line number of the identifier (within the data division).
     * @returns A list of strings which are the ids of the parents of the identifier.
     */
    private fun getParents(lvlNum: Int, line: Int): List<String> {
        return this.symbolTable.getParents(lvlNum, line)
    }

    /**
     * Helpful method to get the children of a record.
     *
     * @param id The identifier of the record.
     * @returns A list of strings which are the ids of the children of the identifier.
     */
    private fun getChildren(id: List<String>, lvlNum: Int): List<List<String>> {
        return this.symbolTable.getChildren(id, lvlNum)
    }

    /**
     * Helpful method to get the full name of an identifier.
     *
     * @param id The identifier of the field (could be full or not)
     * @returns The full name of the identifier as a [List] of [String]s.
     */
    private fun getFullName(id: List<String>): List<String>? {
        return this.symbolTable.getFullIdentifierName(id)
    }

    /**
     * Helpful method to check if identifier is valid.
     *
     * @param id The name of the identifier.
     * @returns True if identifier is valid, false otherwise.
     */
    private fun isValidIdentifier(id: List<String>): Boolean {
        return this.symbolTable.isValidIdentifier(id)
    }

    private fun isValidParagraph(id: List<String>): Boolean{
        return this.symbolTable.isValidParagraph(id)
    }

    /**
     * Helpful method to check if indexed identifier is valid.
     *
     * @param id The name of the identifier.
     * @returns True if indexed identifier is valid, false otherwise.
     */
    private fun isValidIndexedIdentifier(id: List<String>, idx: Int): Boolean {
        return this.symbolTable.isValidIndex(id, idx)
    }

    /**
     * Helpful method to check if identifier is an array.
     *
     * @param id The name of the identifier.
     * @returns True if identifier is an array, false otherwise.
     */
    private fun isArrayIdentifier(id: List<String>): Boolean {
        return this.symbolTable.isArray(id)
    }

    /**
     * Helpful method to check if identifier is record.
     *
     * @param id The name of the identifier.
     * @returns True if identifier is a record, false otherwise.
     */
    private fun isRecordIdentifier(id: List<String>): Boolean {
        return this.symbolTable.isRecord(id)
    }

    /**
     * Helpful method to check if identifier is unambiguous.
     *
     * @param id The name of the identifier.
     * @returns True if identifier is unambiguous, false otherwise.
     */
    private fun isUnambiguousIdentifier(id: List<String>): Boolean {
        return this.symbolTable.isUnambiguous(id)
    }

    /**
     * Helpful method to get BabyCobol [Type] of the identifier.
     *
     * @param id The name of the identifier.
     * @returns [Type] of the identifier.
     */
    private fun getBabyCobolType(id: List<String>): Type? {
        return this.symbolTable.getBabyCobolType(id)
    }

    /**
     * A type checker for a ParseTree node.
     *
     * It checks if the given node has been defined in the [symbolTable] with the correct type.
     * If the node is not defined, it will see this as an error too.
     *
     * @param node The node to check.
     * @param expected The expected type from node1.
     */
    private fun checkType(node: ParserRuleContext, expected: Type) {
        val actual: Type? = getType(node)
        if (actual == null) {
            node.getStart().addError(TypeError) {
                "Missing inferred type of ${node.text}."
            }
        } else if (actual != expected) {
            node.getStart().addError(TypeError) {
                "Expected type '$expected' but found '$actual'."
            }
        }
    }

    /**
     * A numerical type checker for a ParseTree node.
     *
     * It checks if the given node has been defined in the [symbolTable] with a numerical type (INT or DECIMAL).
     * If the node is not defined, it will see this as an error too.
     *
     * @param node The node to check.
     */
    private fun checkNumericalType(node: ParserRuleContext) {
        val actual: Type? = getType(node)
        if (actual == null) {
            node.getStart().addError(TypeError) {
                "Missing inferred type of ${node.text}."
            }
        } else if (actual != Type.INT && actual != Type.DECIMAL) {
            node.getStart().addError(TypeError) {
                "Expected type 'numerical' but found '$actual'."
            }
        }
    }

    /**
     * A type checker for 2 ParseTree nodes.
     *
     * It checks if 2 gives nodes have been defined with the same type.
     * If a node is not defined, it will see this as an error.
     *
     * @param allowBools Allow the types to be booleans.
     * @param node1 The first node.
     * @param node2 The second node.
     * @return Boolean that returns false if no errors were found. Otherwise it is true.
     */
    private fun checkEqualType(node1: ParserRuleContext, node2: ParserRuleContext, allowBools: Boolean): Boolean {
        val first: Type? = getType(node1)
        val second: Type? = getType(node2)
        var result = false
        if (first == null) {
            node1.getStart().addError(TypeError) {
                "Missing inferred type of ${node1.text}."
            }
            result = true
        } else if (first === Type.BOOL && !allowBools) {
            node1.getStart().addError(TypeError) {
                "Operation is not allowed on booleans."
            }
            result = true
        }
        if (second == null) {
            node1.getStart().addError(TypeError) {
                "Missing inferred type of ${node2.text}."
            }
            result = true
        }
        if (first !== second) {
            node1.getStart().addError(TypeError) {
                "Operands should have the same type, but are '$first' and '$second'."
            }
            result = true
        }
        return result
    }
}


