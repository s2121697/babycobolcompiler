package com.gitlab.legoatoom.babycobolcompiler.checker

import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateResult
import com.gitlab.legoatoom.babycobolcompiler.state.BabyCobolCompilerState
import org.antlr.v4.runtime.tree.ParseTree

class CheckerState(private val parseTree: ParseTree) : BabyCobolCompilerState<SymbolTable>("CheckerState") {

    private var checker: Checker = Checker(errors, warnings)

    override fun handle(): StateResult<out SymbolTable> {
        return checker.check(parseTree)
    }
}
