package com.gitlab.legoatoom.babycobolcompiler.checker

import com.gitlab.legoatoom.babycobolcompiler.util.IdentifierInfo
import com.gitlab.legoatoom.babycobolcompiler.util.OrderedHashMap
import com.gitlab.legoatoom.babycobolcompiler.util.ParagraphInfo
import com.gitlab.legoatoom.babycobolcompiler.util.Type
import org.antlr.v4.runtime.tree.ParseTree

/**
 * Class representing a symbol table. It contains hashmaps for the type of ParseTree nodes and a hashmap mapped to
 * from id (List<String>) to identifier info.
 */
class SymbolTable(
    private val types: LinkedHashMap<ParseTree, Type?> = LinkedHashMap(),
    private val identifiers: LinkedHashMap<List<String>, IdentifierInfo> = LinkedHashMap(),
    /**
     * A paragraph has a name, and the paragraph info
     */
    private val paragraphs: OrderedHashMap<String, ParagraphInfo> = OrderedHashMap()
) {

    fun setParagraphCalled(id: String, pair: Pair<Int, Int>) {
        paragraphs.merge(id, ParagraphInfo(id, mutableSetOf(pair) , -1, -1, called = true, defined = false)){ old, _ ->
            old.called = true
            old.callers.add(pair)
            old //return
        }
    }

    fun setParagraphDefined(info: ParagraphInfo) {
        if (!paragraphs.elements.contains(info.name)) paragraphs.elements.add(info.name)
        paragraphs.merge(info.name, info){ old, new ->
            new.called = old.called
            new.callers = old.callers
            new //return
        }
    }

    fun getParagraph(id: String): ParagraphInfo? {
        return paragraphs[id]
    }


    fun getUndefinedParagraphs(): Collection<ParagraphInfo> = paragraphs.filterValues {
            value ->  !value.defined && value.called
    }.values

    fun isValidParagraph(id: List<String>): Boolean {
        return id.size == 1 && paragraphs.containsKey(id.first())
    }

    fun getParagraphs(): List<String> {
        return paragraphs.elements
    }

    /**
     * Setter for a new Type connection.
     * @param node The node key.
     * @param type The type value.
     */
    fun setType(node: ParseTree, type: Type) {
        this.types[node] = type
    }

    /**
     * Getter for a Type connection.
     * @param node The key.
     * @return The Type value.
     */
    fun getType(node: ParseTree): Type? {
        return this.types[node]
    }


    /**
     * Setter for identifier info regex.
     * @param id The id of the identifier.
     * @param type The regex to be set.
     */
    fun setIdentifierRegex(id: List<String>, type: Regex) {
        this.identifiers[id]?.regex = type
    }

    /**
     * Setter for identifier info length.
     * @param id The id of the identifier.
     * @param len The length to be set.
     */
    fun setIdentifierLen(id: List<String>, len: Int) {
        this.identifiers[id]?.len = len
    }

    /**
     * Setter for identifier info padding symbol.
     * @param id The id of the identifier.
     * @param symbol The padding symbol to be set.
     */
    fun setIdentifierPadSymbol(id: List<String>, symbol: Char) {
        this.identifiers[id]?.padSymbol = symbol
    }

    /**
     * Setter for identifier info occurs attribute.
     * @param id The id of the identifier.
     * @param n The occurs value to be set.
     */
    fun setIdentifierOccurs(id: List<String>, n: Int) {
        this.identifiers[id]?.occurs = n
    }

    /**
     * Setter for identifier info return type.
     * @param id The id of the identifier.
     * @param returnType The return type to be set.
     */
    fun setIdentifierReturnType(id: List<String>, returnType: String) {
        this.identifiers[id]?.returnType = returnType
    }

    // TODO: for sufficient qualification match first part of the id with the given identifier.
    /**
     * Getter for the identifier regex.
     * @param id The id of the identifier.
     * @returns The [Regex] which specifies the format of the identifier.
     */
    private fun getIdentifierRegex(id: List<String>): Regex? {
        return this.identifiers[id]?.regex
    }

    /**
     * Getter for the identifier occurs.
     * @param id The id of the identifier.
     * @returns An [Int] which represents the length of the array of the identifier.
     */
    private fun getIdentifierOccurs(id: List<String>): Int? {
        return this.identifiers[id]?.occurs
    }

    /**
     * Getter for the whole identifier info object.
     * @param id The id of the identifier.
     * @returns An [IdentifierInfo] belonging to the identifier.
     */
    fun getIdentifierInfo(id: List<String>): IdentifierInfo? {
        return this.identifiers[id]
    }

    /**
     * Helpful method to get a [Regex] for allowed character for an identifier.
     *
     * @param id The name of the identifier.
     * @return a [Regex] which matches any allowed char.
     */
    private fun getAllowedChars(id: List<String>): String? {
        val sizedRegex = this.identifiers[id]?.regex ?: return null
        val curlyBracketContents = Regex("\\{.+}")
        return curlyBracketContents.replace(sizedRegex.toString(), "").drop(1).dropLast(1)
    }

    /**
     * Helpful method to get the full name of an identifier as a [String].
     * @param id The id of the identifier.
     * @returns A [String] which represents the full name of the identifier.
     */
    fun getFullIdentifierName(id: List<String>): List<String>? {
        val len = id.size
        if (len == 0) return null
        val field = id[0]

        // Check all id's in the symbol table
        for (identifier in this.identifiers.keys) {
            // The field names must be the same.
            if (identifier.first() == field) {
                // If length is only 1 we found it
                if (len == 1) return identifier

                // Loop over the parents of the id. If we cannot find a parent of id in the current
                // identifier we now this is not a match. If all parents of id match at ascending indices, we have
                // found a match.
                var curIdx = 1
                for (parent in id.drop(1)) {
                    // Make a list of indices that contain the parent and get the lowest index greater or equal to
                    // curIdx.
                    val parIndices = identifier.mapIndexedNotNull { index, elem -> index.takeIf { elem == parent } }
                    val parIdx = parIndices.find { it >= curIdx }

                    // If no matching indices were found or they are not greater or equal to the current index, we know
                    // we did not find a match.
                    if (parIndices.isEmpty() || parIdx == null) break

                    // Check if we are done
                    if (parent == id.last()) {
                        return identifier
                    } else {
                        // Update the current index.
                        curIdx = parIdx
                    }
                }
            }
        }
        return null
    }

    /**
     * Helpful method to check if an id is an valid (declared) identifier.
     *
     * @param id The name of the identifier.
     * @return true if id is defined in the symbol table, false otherwise.
     */
    fun isValidIdentifier(id: List<String>): Boolean {
        for (symbolTableId in this.identifiers.keys) {
            if (symbolTableId.containsAll(id)) return true
        }

        return false
    }

    /**
     * Helpful method to check if an id is an valid (declared) identifier.
     *
     * @param id The name of the identifier.
     * @return true if id is defined in the symbol table, false otherwise.
     */
    fun isValidIndex(id: List<String>, idx: Int): Boolean {
        val occurs = getIdentifierOccurs(id)
        if (occurs == null || occurs < idx || idx <= 0) return false

        return true
    }

    /**
     * Helpful method to check if an id is an array.
     *
     * @param id The name of the identifier.
     * @return true if id is an array, false otherwise.
     */
    fun isArray(id: List<String>): Boolean {
        return getIdentifierOccurs(id)!! > 0
    }

    /**
     * Helpful method to check if an id is a record.
     *
     * @param id The name of the identifier.
     * @return true if id is a record, false otherwise.
     */
    fun isRecord(id: List<String>): Boolean {
        return getIdentifierRegex(id) == null
    }

    fun getChildren(parentId: List<String>, lvlNum: Int): List<List<String>> {
        val children: MutableList<List<String>> = mutableListOf()
        val len = parentId.size

        for ((id, _) in this.identifiers) {
            // Child id must always be bigger than parent id
            if (parentId.size >= id.size) {
                continue
            }

            // Check if full parentId is part of the id. If it is, add the id to the children.
            val startIdx = id.indexOf(parentId[0])
            if (startIdx > -1 && id.subList(startIdx, startIdx + len) == parentId) {
                children.add(id)
            }
        }

        return children
    }

    /**
     * Helpful method to check if an id is unambiguous.
     *
     * @param id The name of the identifier.
     * @return true if id is unambiguous, false otherwise.
     */
    fun isUnambiguous(id: List<String>): Boolean {
        val len = id.size
        if (len == 0) return false
        val field = id[0]
        var count = 0;

        // Check all id's in the symbol table
        for (identifier in this.identifiers.keys) {
            // The field names must be the same.
            if (identifier.first() == field) {
                // If length is only 1 we found a match
                if (len == 1) {
                    count++
                    continue
                }

                // Loop over the parents of the id. If we cannot find a parent of id in the current
                // identifier we now this is not a match. If all parents of id match at ascending indices, we have
                // found a match.
                var curIdx = 1
                for (parent in id.drop(1)) {
                    // Make a list of indices that contain the parent and get the lowest index greater or equal to
                    // curIdx.
                    val parIndices = identifier.mapIndexedNotNull { index, elem -> index.takeIf { elem == parent } }
                    val parIdx = parIndices.find { it >= curIdx }

                    // If no matching indices were found or they are not greater or equal to the current index, we know
                    // we did not find a match.
                    if (parIndices.isEmpty() || parIdx == null) break

                    // Check if we are done
                    if (parent == id.last()) {
                        count++
                        break
                    } else {
                        // Update the current index.
                        curIdx = parIdx
                    }
                }
            }
        }

        // If exactly 1 field with the al the parent of id exists -> unambiguous
        if (count == 1) return true
        // If 0 fields are found, then is an unresolved reference
        if (count == 0) return true
        return false
    }

    /**
     * Helpful method to get the BabyCobol type of an identifier.
     *
     * @param id The name of the identifier.
     * @return the [Type] of the identifier.
     */
    fun getBabyCobolType(id: List<String>): Type? {
        // Get the regex of the type of the identifier from the symbol table.
        val regexString: String = this.getAllowedChars(id) ?: return null
        val reg = Regex("^$regexString+$")

        // Determine type of the identifier based on the regex
        if (reg.matches("p")) {
            return Type.STRING
        } else if (reg.matches("0")) {
            return Type.INT
        }

        return null
    }

    /**
     * Create a [String] with all variables declarations separated by newlines.
     *
     * All variables are initialized as a subclass of the [Picture] class. Identifier info is used to supply the object
     * with the necessary info.
     */
    fun makeVarDeclarations(): String {
        var res = ""
        for ((id, info) in this.identifiers) {
            if (info.regex != null) {
                val name = id.joinToString("of").lowercase()
                val parentArrays = getParentArrays(id.drop(1))

                // Construct correct Picture class
                val picClass: String = if (info.returnType == "Int") {
                    "IntPicture(Regex(\"${info.regex}\"), ${info.len}, '${info.padSymbol}', \"_$name\")"
                } else {
                    "StringPicture(Regex(\"${info.regex}\"), ${info.len}, '${info.padSymbol}', \"_$name\")"
                }

                // Add correct initialization to the result string. Val is used for the array declarations so that the
                // size cannot be changed.
                res += if (info.occurs > 0) {
                    // Make the nested arrays from the parents.
                    // Initialization is based on this sample: val sample = Array(10) { Array(10) { Array(10) { 0 } } }
                    if (parentArrays.isNotEmpty()) {
                        var arrayInits = ""
                        var closingBracktes = ""
                        for (p in parentArrays) {
                            arrayInits += " Array($p) {"
                            closingBracktes += " }"
                        }
                        "val _$name =$arrayInits Array(${info.occurs}) { $picClass }$closingBracktes\n"
                    } else {
                        "val _$name = Array(${info.occurs}) { $picClass }\n"
                    }
                } else {
                    // Make the nested arrays from the parents.
                    if (parentArrays.isNotEmpty()) {
                        var arrayInits = ""
                        var closingBracktes = ""
                        for (p in parentArrays) {
                            arrayInits += "Array($p) { "
                            closingBracktes += " }"
                        }
                        "val _$name = $arrayInits$picClass$closingBracktes\n"
                    } else {
                        "var _$name = $picClass\n"
                    }
                }
            }
        }

        return res
    }

    /**
     * Create a [Int] list with the lengths of the parent arrays.
     *
     * This function creates a list with [Int]'s that resembles the lengths of the arrays of the parents of the
     * identifier. It starts with the 'highest' parent that is an array type.
     */
    private fun getParentArrays(parents: List<String>): List<Int> {
        // Initialize list
        val res: MutableList<Int> = mutableListOf()

        if (parents.isNotEmpty()) {
            // Index to start the sublist from. Gets decremented every iteration.
            var startIdx = parents.size

            while (startIdx > 0) {
                // Get the current (parent) id and the length of the array (n)
                val curId: List<String> = parents.subList(startIdx - 1, parents.size)
                val n = getIdentifierOccurs(curId)
                // If is bigger than 1, it is an array type
                if (n != null && n > 1) res.add(n)
                startIdx -= 1
            }
        }

        return res
    }

    // setIdentifier and getParents are used in the data division to set the nesting structure of ids.
    /**
     * Set the identifier info of an identifier.
     *
     * @param id The id of the identifier.
     * @param info The identifier info of the identifier.
     */
    fun setIdentifier(id: List<String>, info: IdentifierInfo) {
        this.identifiers[id] = info
    }

    /**
     * Get parents of a identifier.
     *
     * A parent of the identifier is the lowest data entry still above the current identifier with a lower level
     * number than the current identifier.
     */
    fun getParents(lvlNum: Int, line: Int): List<String> {
        if (line == 0) return emptyList()

        // Search for the parent in the hashmap.
        var parents = emptyList<String>()
        var parentLine: Int = -1
        for ((id, info) in this.identifiers) {
            // Parent is the first data entry above the current one with a lower level number.
            if (info.lvlNum < lvlNum && (line - info.line < line - parentLine)) {
                parents = id
                parentLine = info.line
            }
        }
        // It can happen that the entry does not have a parent, then it is a top-level data entry.
        return parents
    }
}
