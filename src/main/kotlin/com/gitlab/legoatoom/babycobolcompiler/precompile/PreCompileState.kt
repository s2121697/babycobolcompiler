package com.gitlab.legoatoom.babycobolcompiler.precompile

import com.gitlab.legoatoom.babycobolcompiler.argparser.Settings
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateError
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateResult
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateSuccess
import com.gitlab.legoatoom.babycobolcompiler.exceptions.UnknownStatusIndicator
import com.gitlab.legoatoom.babycobolcompiler.state.BabyCobolCompilerState

class PreCompileState(private val settings: Settings) : BabyCobolCompilerState<String>("PreCompiler") {


    /**
     * Position Based Parsing and Line Continuations/
     *
     * From the code, we remove comments and add line continuations.
     * The first six columns is the sequence number, and can be ignored.
     * The seventh column is the line status indicator.
     *
     */
    override fun handle(): StateResult<out String> {
        val file = settings.sourceFile
        val code: StringBuilder = StringBuilder()

        var i = 0
        file.forEachLine {
            val line = it.substring(6, 72.coerceAtMost(it.length))
            when (val lineStatus = line.first()) {
                ' ' -> code.append(System.lineSeparator()).append(line.substring(1))
                '-' -> code.append(' ').append(line.substring(1))
                '*' -> { /* Maybe we do something with comments later */
                }
                else -> warnings.add(UnknownStatusIndicator to "Unknown: '$lineStatus', skipping..")
            }
            i += 1
        }

        // Check of errors.
        if (this.errors.isNotEmpty()) {
            return StateError(getErrors()!!, getWarnings())
        }

        return StateSuccess(code.toString(), getWarnings())
    }
}
