package com.gitlab.legoatoom.babycobolcompiler.runner

import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateException
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateResult
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateSuccess
import com.gitlab.legoatoom.babycobolcompiler.state.BabyCobolCompilerState
import org.jetbrains.kotlin.cli.common.environment.setIdeaIoUseFallback
import javax.script.ScriptEngineManager

class RunnerState(private val codeString: String) : BabyCobolCompilerState<Unit>("RunnerState") {
    override fun handle(): StateResult<out Unit> {
        println("Running...")
        println()
        return try{
            setIdeaIoUseFallback()
            val manager = ScriptEngineManager()
            val engine = manager.getEngineByExtension("kts")
            engine.eval(codeString)
            StateSuccess(Unit)
        } catch (e: Exception) {
            StateException(e)
        }
    }
}
