package com.gitlab.legoatoom.babycobolcompiler.util

import org.antlr.v4.runtime.BaseErrorListener
import org.antlr.v4.runtime.RecognitionException
import org.antlr.v4.runtime.Recognizer
import org.antlr.v4.runtime.misc.ParseCancellationException
import org.junit.jupiter.api.Assertions

//
fun convertionEquals(babyCobolInput: String, kotlinExpectedOutput: String) {
    Assertions.assertEquals(
        "\n" + kotlinExpectedOutput + "\n\n\n\n" + pictureClass,
        getConvertedProgram(babyCobolInput)
    )
}

//
///**
// * An ErrorListener for lexer errors.
// *
// * An instance of this class catches all errors and throws a LexerException if it catches one.
// *
// * @property INSTANCE instance of LexerErrorListener.
// */
class LexerErrorListener : BaseErrorListener() {
    @Throws(ParseCancellationException::class)
    override fun syntaxError(
        recognizer: Recognizer<*, *>?,
        offendingSymbol: Any,
        line: Int,
        charPositionInLine: Int,
        msg: String,
        e: RecognitionException?
    ) {
//        throw LexerException("line $line:$charPositionInLine $msg")
    }

    companion object {
        val INSTANCE = LexerErrorListener()
    }
}

//
///**
// * An ErrorListener for parse errors.
// *
// * An instance of this class catches all errors and throws a ParserException if it catched one.
// *
// * @property INSTANCE instance of ParserErrorListener.
// */
class ParserErrorListener : BaseErrorListener() {
    @Throws(ParseCancellationException::class)
    override fun syntaxError(
        recognizer: Recognizer<*, *>?,
        offendingSymbol: Any,
        line: Int,
        charPositionInLine: Int,
        msg: String,
        e: RecognitionException?
    ) {
//        throw ParserException("line $line:$charPositionInLine $msg")
    }

    companion object {
        val INSTANCE = ParserErrorListener()
    }
}

//
///**
// * Convert a BabyCobol program to a Kotlin program.
// *
// * @param progString a String with a BabyCobol program.
// * @return a String with a Kotlin program representing the progString program.
// * @throws LexerException if progString cannot be lexically analyzed.
// * @throws ParserException if progString cannot be parsed.
// */
//@Suppress("RemoveRedundantQualifierName")
fun getConvertedProgram(progString: String): String {
//    val inStream: InputStream = ByteArrayInputStream(progString.toByteArray())
//
//    val lexer = BabyCobolVocabular(CharStreams.fromStream(inStream))
//    lexer.removeErrorListeners()
//    lexer.addErrorListener(LexerErrorListener.INSTANCE)
//    val tokens = CommonTokenStream(lexer)
//    val parser = BabyCobol(tokens)
//    parser.removeErrorListeners()
//    parser.addErrorListener(ParserErrorListener.INSTANCE)
//    val tree = parser.program()
//
//    val symbolTable = SymbolTable()
//
//    val checker = Checker(symbolTable)
//    checker.check(tree)
//
//    return Convertor(symbolTable).convert(tree)
    return "a"
}
