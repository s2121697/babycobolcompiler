package com.gitlab.legoatoom.babycobolcompiler.util

import org.intellij.lang.annotations.Language

@Language("kotlin")
val pictureClass: String = """
            abstract class Picture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) {
                var internalValue: String = padSymbol.toString().repeat(len)
            
                abstract fun getValue(): Any
                abstract fun setValue(newVal: String)
                override fun toString(): String { return this.internalValue }
            }
            
            class IntPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
                override fun getValue(): java.math.BigInteger {
                    return super.internalValue.toBigInteger()
                }
            
                override fun setValue(newVal: String) {
                    // Check length of newVal. Pad to left with 0's if length is smaller, take the first n numbers if 
                    // the length is bigger.
                    var updatedVal: String = newVal
                    if (newVal.length < len) {
                        updatedVal = newVal.padStart(len, padSymbol)
                    } else if (newVal.length > len) {
                        updatedVal = newVal.takeLast(len)
                    }
            
                    // Check if value correspond with regex
                    if (regex.matches(updatedVal)) this.internalValue = updatedVal
                    else {
                        val errMessage = "Cannot assign " + updatedVal + " to data entry " + this.name
                        throw Exception(errMessage)
                    }
                }
            }
            
            class StringPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
                override fun getValue(): String {
                    return super.internalValue
                }
            
                override fun setValue(newVal: String) {
                    // Check length of newVal. Pad to right with spaces if length is smaller, take the last n characters if 
                    // the length is bigger.
                    var updatedVal: String = newVal
                    if (newVal.length < len) {
                        updatedVal = newVal.padEnd(len, padSymbol)
                    } else if (newVal.length > len) {
                        updatedVal = newVal.takeLast(len)
                    }
            
                    // Check if value correspond with regex
                    if (regex.matches(updatedVal)) this.internalValue = updatedVal
                    else {
                        val errMessage = "Cannot assign " + updatedVal + " to data entry " + this.name
                        throw Exception(errMessage)
                    }
                }
            }
        """.trimIndent()