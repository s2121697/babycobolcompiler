package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertContains

internal class CaseInsensitivityOutputTest : OutputTestSetup() {
    @Test
    fun allLowercaseProgram() {
        assertEquals("Hello World!\n", getOutput("$outputDir/allLowercase"))
    }

    @Test
    fun lowercasePictureNotAllowed() {
        assertContains(getError("$outputDir/lowercasePictureNotAllowed"), "InvalidPictureError")
    }
}