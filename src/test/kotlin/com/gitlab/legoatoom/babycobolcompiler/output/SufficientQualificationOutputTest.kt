package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertContains

internal class SufficientQualificationOutputTest: OutputTestSetup() {
    @Test
    fun sufficientQualifications() {
        assertEquals("001\n002\n001\n001\n001\n002\n002\n002\n", getOutput("$outputDir/sufficientQualifications"))
    }

    @Test
    fun insufficientQualification1() {
        assertContains(getError("$outputDir/insufficientQualification1"), "AmbiguousIdentifierError")
    }

    @Test
    fun insufficientQualification2() {
        assertContains(getError("$outputDir/insufficientQualification2"), "AmbiguousIdentifierError")
    }

    @Test
    fun insufficientQualification3() {
        assertContains(getError("$outputDir/insufficientQualification3"), "AmbiguousIdentifierError")
    }
}