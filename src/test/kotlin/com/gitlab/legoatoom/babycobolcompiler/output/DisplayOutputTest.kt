package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals

internal class DisplayOutputTest : OutputTestSetup() {
    @Test
    fun emptyDisplay() {
        assertEquals("\n", getOutput("$outputDir/emptyDisplay"))
    }

    @Test
    fun textLiteralDisplay() {
        assertEquals("Hello World!\n", getOutput("$outputDir/simpleTextLiteralDisplay"))
    }

    @Test
    fun intLiteralDisplay() {
        assertEquals("42\n", getOutput("$outputDir/simpleIntLiteralDisplay"))
    }

    @Test
    fun withAdvancingDisplay() {
        assertEquals("42\n1005\n", getOutput("$outputDir/withAdvancingDisplay"))
    }

    @Test
    fun withNoAdvancingDisplay() {
        assertEquals("421005", getOutput("$outputDir/withNoAdvancingDisplay"))
    }

    @Test
    fun delimitedBySizeDisplay() {
        assertEquals("Hello World!\n", getOutput("$outputDir/delimitedBySizeDisplay"))
    }

    @Test
    fun delimitedBySpaceDisplay() {
        assertEquals("Hello World!\n", getOutput("$outputDir/delimitedBySpaceDisplay"))
    }

    @Test
    fun delimitedByLiteralDisplay() {
        assertEquals("Hello World!\nHell\n", getOutput("$outputDir/delimitedByLiteralDisplay"))
    }

    @Test
    fun multipleValuesDisplay() {
        assertEquals("421000BAR", getOutput("$outputDir/multipleValuesDisplay"))
    }

    @Test
    fun identifierDisplay() {
        assertEquals("00022\n00000\n          \n", getOutput("$outputDir/identifierDisplay"))
    }
}