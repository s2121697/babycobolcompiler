package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

internal class SubtractOutputTest : OutputTestSetup() {
    @Test
    fun simpleSubtract() {
        assertEquals("00033\n", getOutput("$outputDir/simpleSubtract"))
    }

    @Test
    fun multipleSubtracts() {
        assertEquals("00026\n", getOutput("$outputDir/multipleSubtracts"))
    }

    @Test
    fun subtractMissingGiving() {
        assertContains(getError("$outputDir/subtractNoGiving"), "MissingGivingError")
    }

    @Test
    fun subtractIncorrectTypes() {
        assertContains(getError("$outputDir/subtractIncorrectTypes"), "TypeError")
    }

    @Test
    fun subtractIdentifier() {
        assertContains(getError("$outputDir/subtractUnknownIdentifier"), "UnresolvedReferenceError")
    }
}