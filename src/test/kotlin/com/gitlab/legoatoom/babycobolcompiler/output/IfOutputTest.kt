package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IfOutputTest : OutputTestSetup() {
    @Test
    fun simpleIf() {
        assertEquals("TRUE\n", getOutput("$outputDir/simpleIf"))
    }

    @Test
    fun simpleIfElse() {
        assertEquals("FALSE IS NOT TRUE\n", getOutput("$outputDir/simpleIfElse"))
    }

    @Test
    fun ifComparison() {
        assertEquals("10 > 8\n10 >= 10\n8 < 10\n8 <= 10\n8 = 8\n", getOutput("$outputDir/ifComparison"))
    }

    @Test
    fun ifBooleanOperators() {
        assertEquals(
            "TRUE AND TRUE\nTRUE OR FALSE\nTRUE OR TRUE\nTRUE XOR FALSE\n",
            getOutput("$outputDir/ifBooleanOperators")
        )
    }

    @Test
    fun ifPrecedence() {
        assertEquals("5 + 5 * 5 ** 2 = 130\n", getOutput("$outputDir/ifPrecedence"))
    }

    @Test
    fun ifIdentifier() {
        assertEquals("A > 8\n", getOutput("$outputDir/ifIdentifier", stdinLines = listOf("10")))
    }
}