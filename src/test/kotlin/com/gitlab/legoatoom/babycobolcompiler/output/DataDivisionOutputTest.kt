package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

internal class DataDivisionOutputTest : OutputTestSetup() {
    @Test
    fun nestedDataEntries() {
        assertEquals("     \n", getOutput("$outputDir/nestedDataEntries"))
    }

    @Test
    fun invalidPicture() {
        assertContains(getError("$outputDir/invalidCharacterInPicture"), "InvalidPictureError")
    }

    @Test
    fun invalidLevelNumber() {
        assertContains(getError("$outputDir/invalidLevelNumber"), "InvalidLevelNumberError")
    }

    @Test
    fun levelNumberLowerThanToplevel() {
        assertContains(getError("$outputDir/levelNumberLowerThanToplevel"), "InvalidLevelNumberError")
    }

//    @Test
//    fun noIdentificationProcedure() {
//        assertContains(getError("$outputDir/noIdentificationProcedure"), "InvalidSyntaxError")
//    }
}