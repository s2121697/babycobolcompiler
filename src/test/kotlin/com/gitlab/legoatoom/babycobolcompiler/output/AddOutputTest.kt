package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

internal class AddOutputTest : OutputTestSetup() {
    @Test
    fun simpleAdd() {
        assertEquals("00213\n", getOutput("$outputDir/simpleAdd"))
    }

    @Test
    fun multipleAdds() {
        assertEquals("07336\n", getOutput("$outputDir/multipleAdds"))
    }

    @Test
    fun addMissingGiving() {
        assertContains(getError("$outputDir/addNoGiving"), "MissingGivingError")
    }

    @Test
    fun addIncorrectTypes() {
        assertContains(getError("$outputDir/addIncorrectTypes"), "TypeError")
    }

    @Test
    fun unknownIdentifier() {
        assertContains(getError("$outputDir/addUnknownIdentifier"), "UnresolvedReferenceError")
    }
}