package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals

internal class EvaluateOutputTest : OutputTestSetup() {
    @Test
    fun evaluateAllOperators() {
        assertEquals("Correct operator precedence!\n", getOutput("$outputDir/evaluateAllOperators"))
    }

    @Test
    fun evaluateNoWhenOther() {
        assertEquals("", getOutput("$outputDir/evaluateNoWhenOther"))
    }

    @Test
    fun evaluateIdentifiers() {
        assertEquals(
            "Whitespace is taken into account!\n",
            getOutput("$outputDir/evaluateIdentifiers", stdinLines = listOf("AAA"))
        )
    }

    @Test
    fun evaluateMultipleStats() {
        assertEquals("100\nThe fourth statement in WHEN-block\n", getOutput("$outputDir/evaluateMultipleStats"))
    }
}