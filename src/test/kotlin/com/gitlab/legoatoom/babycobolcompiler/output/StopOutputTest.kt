package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals

internal class StopOutputTest : OutputTestSetup() {
    @Test
    fun stopBeforeDisplays() {
        assertEquals("", getOutput("$outputDir/stopBeforeDisplays"))
    }

    @Test
    fun stopBetweenDisplays() {
        assertEquals("Hello World!\n", getOutput("$outputDir/stopBetweenDisplays"))
    }

    @Test
    fun stopAfterDisplays() {
        assertEquals("Hello World!\n100\n", getOutput("$outputDir/stopAfterDisplays"))
    }
}