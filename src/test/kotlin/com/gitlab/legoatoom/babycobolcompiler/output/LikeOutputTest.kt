package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertContains

internal class LikeOutputTest: OutputTestSetup() {
    @Test
    fun simpleLike() {
        assertEquals("00022\n00022\n", getOutput("$outputDir/simpleLike"))
    }

    @Test
    fun likeDoesNotTakeOccurs() {
        assertEquals("00022\n", getOutput("$outputDir/likeDoesNotTakeOccurs"))
    }

    @Test
    fun likeOnRecord() {
        assertEquals("022\n000\n", getOutput("$outputDir/likeOnRecord"))
    }

    @Test
    fun invalidIdentifierLike() {
        assertContains(getError("$outputDir/invalidIdentifierLike"), "UnresolvedReferenceError")
    }

    @Test
    fun identifierWithIndexLike() {
        assertContains(getError("$outputDir/identifierWithIndexLike"), "InvalidLikeIdentifierError")
    }
}