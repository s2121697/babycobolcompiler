package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertContains

internal class OccursOutputTest : OutputTestSetup() {
    @Test
    fun simpleOccurs() {
        assertEquals("000\n015\n", getOutput("$outputDir/simpleOccurs"))
    }

    @Test
    fun occursAfterPicture() {
        assertContains(getError("$outputDir/occursAfterPicture"), "InvalidOccursError")
    }

    @Test
    fun arrayNoIndex() {
        assertContains(getError("$outputDir/arrayNoIndex"), "UnresolvedReferenceError")
    }

    @Test
    fun indexOnNoArray() {
        assertContains(getError("$outputDir/indexOnNoArray"), "TypeError")
    }

    @Test
    fun indexOutOfBounds() {
        assertContains(getError("$outputDir/indexOutOfBounds"), "OutOfBoundError")
    }
}