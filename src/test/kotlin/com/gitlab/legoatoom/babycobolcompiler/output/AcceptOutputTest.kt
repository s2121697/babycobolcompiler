package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertContains

internal class AcceptOutputTest : OutputTestSetup() {
    @Test
    fun simpleAdd() {
        assertEquals("00099\n", getOutput("$outputDir/simpleAccept", stdinLines = listOf("99")))
    }

    @Test
    fun multipleAdds() {
        assertEquals("00099\nHello\n", getOutput("$outputDir/multipleAccepts", stdinLines = listOf("99", "Hello")))
    }

    @Test
    fun incompatibleUserInput() {
        assertContains(getError("$outputDir/acceptIncompatibleUserInput", stdinLines = listOf("Hello")), "Exception")
    }
}