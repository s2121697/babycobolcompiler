package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals

internal class PositionBasedParsingLineContinuation : OutputTestSetup() {
    @Test
    fun commentsIgnored() {
        assertEquals("Actual statement\n", getOutput("$outputDir/commentsIgnored"))
    }

    @Test
    fun lineContinuation() {
        assertEquals(
            "This is a very long display statement to show that th this is the line continuation\n",
            getOutput("$outputDir/lineContinuation")
        )
    }
}