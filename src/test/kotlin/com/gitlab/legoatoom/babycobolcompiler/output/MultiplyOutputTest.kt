package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

internal class MultiplyOutputTest : OutputTestSetup() {
    @Test
    fun simpleMultiply() {
        assertEquals("00360\n", getOutput("$outputDir/simpleMultiply"))
    }

    @Test
    fun multipleMultiplies() {
        assertEquals("02520\n", getOutput("$outputDir/multipleMultiplies"))
    }

    @Test
    fun multiplyMissingGiving() {
        assertContains(getError("$outputDir/multiplyNoGiving"), "MissingGivingError")
    }

    @Test
    fun multiplyIncorrectTypes() {
        assertContains(getError("$outputDir/multiplyIncorrectTypes"), "TypeError")
    }

    @Test
    fun multiplyIdentifier() {
        assertContains(getError("$outputDir/multiplyUnknownIdentifier"), "UnresolvedReferenceError")
    }
}