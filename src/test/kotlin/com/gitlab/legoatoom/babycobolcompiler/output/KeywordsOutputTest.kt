package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertEquals

internal class KeywordsOutputTest : OutputTestSetup() {
    @Test
    fun displayDisplay() {
        assertEquals("DISPLAY\n", getOutput("$outputDir/displayDisplay"))
    }

    @Test
    fun ifThenIsElse() {
        assertEquals("THEN is not ELSE\n", getOutput("$outputDir/ifThenIsElse", stdinLines = listOf("66", "67")))
    }
}