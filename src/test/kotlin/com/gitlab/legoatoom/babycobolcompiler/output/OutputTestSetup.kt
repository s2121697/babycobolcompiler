package com.gitlab.legoatoom.babycobolcompiler.output

import com.github.stefanbirkner.systemlambda.SystemLambda.*
import com.gitlab.legoatoom.babycobolcompiler.BabyCobolController
import java.nio.file.Path

internal open class OutputTestSetup {
    protected val outputDir = Path
        .of("src/test/kotlin/com/gitlab/legoatoom/babycobolcompiler/output/bclfiles")
        .toAbsolutePath().toString()

    protected fun getOutput(
        fileName: String,
        stdinLines: List<String> = emptyList(),
        catchExit: Boolean = false
    ): String {
        if (catchExit) {
            val output: String = if (stdinLines.isNotEmpty()) {
                tapSystemOutNormalized {
                    catchSystemExit {
                        withTextFromSystemIn(*stdinLines.toTypedArray()).execute {
                            BabyCobolController().execute(
                                arrayOf("--run", "$fileName.bcl")
                            )
                        }
                    }
                }
            } else {
                tapSystemOutNormalized {
                    catchSystemExit {
                        BabyCobolController().execute(
                            arrayOf(
                                "--run",
                                "$fileName.bcl"
                            )
                        )
                    }
                }
            }
            return output.replace("Running...\n\n", "")
        }

        val output: String = if (stdinLines.isNotEmpty()) {
            tapSystemOutNormalized {
                withTextFromSystemIn(*stdinLines.toTypedArray()).execute {
                    BabyCobolController().execute(
                        arrayOf("--run", "$fileName.bcl")
                    )
                }
            }
        } else {
            tapSystemOutNormalized { BabyCobolController().execute(arrayOf("--run", "$fileName.bcl")) }
        }
        return output.replace("Running...\n\n", "")
    }

    protected fun getError(fileName: String, stdinLines: List<String> = emptyList()): String {
        if (stdinLines.isNotEmpty()) return tapSystemErrNormalized {
            catchSystemExit {
                withTextFromSystemIn(*stdinLines.toTypedArray()).execute {
                    BabyCobolController().execute(
                        arrayOf("--run", "$fileName.bcl")
                    )
                }
            }
        }
        return tapSystemErrNormalized {
            catchSystemExit {
                BabyCobolController().execute(
                    arrayOf(
                        "--run",
                        "$fileName.bcl"
                    )
                )
            }
        }
    }
}