package com.gitlab.legoatoom.babycobolcompiler.output

import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

internal class DivideOutputTest : OutputTestSetup() {
    @Test
    fun simpleDivide() {
        assertEquals("00015\n", getOutput("$outputDir/simpleDivide"))
    }

    @Test
    fun multipleDivides() {
        assertEquals("00005\n", getOutput("$outputDir/multipleDivides"))
    }

    @Test
    fun divideWithRemainder() {
        assertEquals("00010\n", getOutput("$outputDir/divideWithRemainder"))
    }

    @Test
    fun divideWithGivingAndRemainder() {
        assertEquals("00005\n010\n", getOutput("$outputDir/divideWithGivingAndRemainder"))
    }

    @Test
    fun divideMissingGiving() {
        assertContains(getError("$outputDir/divideNoGiving"), "MissingGivingError")
    }

    @Test
    fun divideIncorrectTypes() {
        assertContains(getError("$outputDir/divideIncorrectTypes"), "TypeError")
    }

    @Test
    fun unknownIdentifier() {
        assertContains(getError("$outputDir/divideUnknownIdentifier"), "UnresolvedReferenceError")
    }

    @Test
    fun invalidRemainderIdentifier() {
        assertContains(getError("$outputDir/invalidRemainderIdentifier"), "UnresolvedReferenceError")
    }
}