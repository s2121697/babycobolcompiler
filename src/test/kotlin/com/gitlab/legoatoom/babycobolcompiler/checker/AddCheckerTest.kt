package com.gitlab.legoatoom.babycobolcompiler.checker

import com.gitlab.legoatoom.babycobolcompiler.exceptions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertContains
import kotlin.test.fail

internal class AddCheckerTest : CheckerTestSetup() {

    @Test
    fun correctAdd() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                01 TEST PICTURE IS 999.
            PROCEDURE DIVISION.
                ADD 8 TO TEST.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun correctAddWithGiving() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                01 TEST PICTURE IS 999.
            PROCEDURE DIVISION.
                ADD 8 TO 9 GIVING TEST.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun noGiving() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                01 TEST PICTURE IS 999.
            PROCEDURE DIVISION.
                ADD 8 TO 9.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                assertContains(result.error.errors.faultTypes(), MissingGiving)
            }
        }
    }

    @Test
    fun differentTypes() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                ADD 8 TO 9.9 GIVING TEST.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                assertContains(result.error.errors.faultTypes(), TypeError)
            }
        }
    }

    @Test
    fun nonNumericalVariableWithGiving() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                01 TEST PICTURE IS XXXXX.
            PROCEDURE DIVISION.
                ADD 8 TO 9 GIVING TEST.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                print(result.error.errors.faultTypes())
                assertContains(result.error.errors.faultTypes(), TypeError)
            }
        }
    }

    @Test
    fun uninitializedVars() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                ADD NUM TO TEST.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                print(result.error.errors.faultTypes())
                assertContains(result.error.errors.faultTypes(), UnresolvedReference)
            }
        }
    }

}
