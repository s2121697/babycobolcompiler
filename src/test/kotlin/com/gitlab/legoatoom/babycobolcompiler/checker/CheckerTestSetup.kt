package com.gitlab.legoatoom.babycobolcompiler.checker

import com.gitlab.legoatoom.babycobolcompiler.antlr.AntlrState
import com.gitlab.legoatoom.babycobolcompiler.exceptions.FaultInstance
import com.gitlab.legoatoom.babycobolcompiler.exceptions.StateSuccess
import org.antlr.v4.runtime.tree.ParseTree
import org.jetbrains.annotations.TestOnly
import kotlin.test.fail

//package com.gitlab.legoatoom.babycobolcompiler.checker

internal open class CheckerTestSetup {

    protected fun createParseTree(progString: String): ParseTree {
        try {
            val result = AntlrState(progString).handle()
            if (result !is StateSuccess) fail("Antlr State Failed, check Antlr Tests!")
            return result.value
        } catch (e: Exception) {
            fail("Antlr State Failed, check Antlr Tests!")
        }
    }


    @TestOnly
    protected fun List<FaultInstance>.faultTypes(): List<Pair<Int, String>> = map { pair -> pair.first }
}

///**
// * An ErrorListener for lexer errors.
// *
// * An instance of this class catches all errors and throws a LexerException if it catches one.
// *
// * @property INSTANCE instance of LexerErrorListener.
// */
//class LexerErrorListener : BaseErrorListener() {
////    @Throws(ParseCancellationException::class)
////    override fun syntaxError(
////        recognizer: Recognizer<*, *>?,
////        offendingSymbol: Any,
////        line: Int,
////        charPositionInLine: Int,
////        msg: String,
////        e: RecognitionException
////    ) {
////        throw LexerException("line $line:$charPositionInLine $msg")
////    }
////
////    companion object {
////        val INSTANCE = LexerErrorListener()
////    }
//}
//
///**
// * An ErrorListener for parse errors.
// *
// * An instance of this class catches all errors and throws a ParserException if it catched one.
// *
// * @property INSTANCE instance of ParserErrorListener.
// */
//class ParserErrorListener : BaseErrorListener() {
//    @Throws(ParseCancellationException::class)
//    override fun syntaxError(
//        recognizer: Recognizer<*, *>?,
//        offendingSymbol: Any,
//        line: Int,
//        charPositionInLine: Int,
//        msg: String,
//        e: RecognitionException
//    ) {
//        throw ParserException("line $line:$charPositionInLine $msg")
//    }
//
//    companion object {
//        val INSTANCE = ParserErrorListener()
//    }
//}
//
///**
// * Convert a BabyCobol program to a Kotlin program.
// *
// * @param progString a String with a BabyCobol program.
// * @return true if the input was parsed and checked without exceptions.
// * @throws LexerException if progString cannot be lexically analyzed.
// * @throws ParserException if progString cannot be parsed.
// * @throws CheckerException if progString can be parsed but contains semantic errors.
// */
//@Suppress("RemoveRedundantQualifierName")
//fun tryParseAndCheck(progString: String): Boolean {
//    val inStream: InputStream = ByteArrayInputStream(progString.toByteArray())
//
//    val lexer = BabyCobolVocabular(CharStreams.fromStream(inStream))
//    lexer.removeErrorListeners()
//    lexer.addErrorListener(LexerErrorListener.INSTANCE)
//    val tokens = CommonTokenStream(lexer)
//    val parser = BabyCobol(tokens)
//    parser.removeErrorListeners()
//    parser.addErrorListener(ParserErrorListener.INSTANCE)
//    val tree = parser.program()
//
//    val checker = Checker(SymbolTable())
//    checker.check(tree)
//
//    return true
//}
