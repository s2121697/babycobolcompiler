package com.gitlab.legoatoom.babycobolcompiler.checker

import com.gitlab.legoatoom.babycobolcompiler.exceptions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertContains
import kotlin.test.fail

internal class DataDivisionCheckerTest: CheckerTestSetup() {

    @Test
    fun oneDigitLvlNum() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                8 TEST PICTURE IS X.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                assertContains(result.error.errors.faultTypes(), InvalidLevelNumber)
            }
        }
    }

    @Test
    fun threeDigitLvlNum() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                088 TEST PICTURE IS X.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                assertContains(result.error.errors.faultTypes(), InvalidLevelNumber)
            }
        }
    }

    @Test
    fun twoDigitLvlNum() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                08 TEST PICTURE IS X.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun increasingLvlNum() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                08 TEST PICTURE IS X.
                    10 VAR PICTURE IS 99.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun decreasingLvlNum() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                18 TEST PICTURE IS X.
                    10 VAR PICTURE IS 99.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                assertContains(result.error.errors.faultTypes(), InvalidLevelNumber)
            }
        }
    }

    @Test
    fun wrongRepresentationString() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                18 TEST PICTURE IS LL.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                assertContains(result.error.errors.faultTypes(), InvalidPicture)
            }
        }
    }

    @Test
    fun negativeOccurs() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                01 TEST PICTURE IS XX OCCURS -10 TIMES.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                assertContains(result.error.errors.faultTypes(), InvalidOccurs)
            }
        }
    }

    @Test
    fun occursAfterPicture() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                01 TEST PICTURE IS XX OCCURS 10 TIMES.
                    05 N PICTURE IS 99 OCCURS 8 TIMES.
        """.trimIndent()

        when (val result: StateResult<out SymbolTable> = CheckerState(createParseTree(prog)).handle()) {
            is StateFailure, is StateException -> {
                validate(result)
                fail("Checker found a bigger problem")
            }
            is StateSuccess -> {
                fail("Checker did not the error we expected")
            }
            is StateError -> {
                assertContains(result.error.errors.faultTypes(), InvalidOccurs)
            }
        }
    }

    @Test
    fun simpleArray() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                01 TEST PICTURE IS XX OCCURS 10 TIMES.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun nestedArray() {
        val prog = """
            IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            DATA DIVISION.
                01 X OCCURS 10 TIMES.
                    05 Y OCCURS 10 TIMES.
                        09 Z PICTURE IS X OCCURS 10 TIMES.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }
}