package com.gitlab.legoatoom.babycobolcompiler.checker

import com.gitlab.legoatoom.babycobolcompiler.exceptions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertContains
import kotlin.test.fail

internal class DispExprCheckerTest : CheckerTestSetup() {
    @Test
    fun onlyDelimited() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun onlyBy() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 BY.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun noDelimiter() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun tooManyDelimiters1() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY SPACE SIZE.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun tooManyDelimiters2() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY SPACE 7.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }


    @Test
    fun tooManyDelimiters3() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY SIZE SPACE.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun tooManyDelimiters4() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY SIZE 7.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun tooManyDelimiters5() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY 7 SPACE.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun tooManyDelimiters6() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY 7 SIZE.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun tooManyDelimiters7() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY SPACE 7 SIZE.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun delimiterButNoDelimitedBy1() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 SIZE.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun delimiterButNoDelimitedBy2() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 SPACE.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun fullDelimitedByAndDelimiter() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 DELIMITED BY SPACE.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun displayTwoLiterals() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 7.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }
}
