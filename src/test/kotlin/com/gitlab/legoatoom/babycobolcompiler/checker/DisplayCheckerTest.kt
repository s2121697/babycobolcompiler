package com.gitlab.legoatoom.babycobolcompiler.checker

import com.gitlab.legoatoom.babycobolcompiler.exceptions.*
import org.junit.jupiter.api.Test
import kotlin.test.assertContains
import kotlin.test.fail

internal class DisplayCheckerTest : CheckerTestSetup() {

    @Test
    fun withMissing() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 NO ADVANCING.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun noMissing() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 WITH ADVANCING.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun advancingMissing() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 WITH NO.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun wrongOrderWithNoAdvancing1() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 WITH ADVANCING NO.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun wrongOrderWithNoAdvancing2() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 ADVANCING WITH NO.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun wrongOrderWithNoAdvancing3() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 ADVANCING NO WITH.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun wrongOrderWithNoAdvancing4() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 NO ADVANCING WITH.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun wrongOrderWithNoAdvancing5() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 NO WITH ADVANCING.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun displayWithoutAdvancing() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }

    @Test
    fun displayCorrectWithNoAdvancing() {
        val prog = """
             IDENTIFICATION DIVISION.
                PROGRAM-ID. TEST.
            PROCEDURE DIVISION.
                DISPLAY 8 WITH NO ADVANCING.
        """.trimIndent()

        val result = CheckerState(createParseTree(prog)).handle()

        if (result !is StateSuccess) {
            validate(result)
            fail("Checker found errors where there shouldn't be any")
        }
    }
}
