package com.gitlab.legoatoom.babycobolcompiler.ir

import com.gitlab.legoatoom.babycobolcompiler.checker.SymbolTable
import com.gitlab.legoatoom.babycobolcompiler.util.IdentifierInfo
import com.gitlab.legoatoom.babycobolcompiler.util.OrderedHashMap
import com.gitlab.legoatoom.babycobolcompiler.util.ParagraphInfo
import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRParagraphTest : IRTestSetup() {
    @Test
    fun simpleParagraphs() {
        val orderMap = OrderedHashMap<String, ParagraphInfo>()
        orderMap["SUM"] = ParagraphInfo("SUM", mutableSetOf(), 1, 1, false, true)
        orderMap["DIFFERENCE"] = ParagraphInfo("DIFFERENCE", mutableSetOf(), 1, 1, false, true)
        orderMap["DISPLAYFUNC"] = ParagraphInfo("DISPLAYFUNC", mutableSetOf(), 1, 1, false, true)
        val symbolTable = SymbolTable(
            paragraphs = orderMap
        )

        val sentences = IRSentence(
            listOf(
                IRAdd(
                    IRLiteral.IRIntLit(10.toBigInteger()),
                    IRIdentifier("TEST", ""),
                    null, false, true
                )
            )
        )

        val paragraphs = listOf(
            IRParagraph(
                IRIdentifier("SUM", ""), listOf(
                    IRSentence(
                        listOf(
                            IRAdd(
                                IRIdentifier("VAR1", ""),
                                IRIdentifier("TEST", ""),
                                null, true, true
                            )
                        )
                    )
                )
            ),
            IRParagraph(
                IRIdentifier("DIFFERENCE", ""), listOf(
                    IRSentence(
                        listOf(
                            IRSubtract(
                                IRIdentifier("VAR1", ""),
                                IRIdentifier("TEST", ""),
                                null, true, true
                            )
                        )
                    )
                )
            ),
            IRParagraph(
                IRIdentifier("DISPLAYFUNC", ""), listOf(
                    IRSentence(
                        listOf(
                            IRDisplay(
                                listOf(IRDispExpr(IRIdentifier("TEST", ""))),
                                false
                            )
                        )
                    )
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences), paragraphs, symbolTable)

        val stats = """
            _test.setValue((10.toBigInteger().plus(_test.getValue())).toString())
        """.trimIndent()

        val paragraphString = paragraphs.joinToString(";", transform = IRParagraph::makeParagraph)
        val paragraphsCall = "   _sum();_difference();_displayfunc()"

        assertEquals(getExpectedOutput(stats = stats, paragraphs = paragraphString, paragraphsCall = paragraphsCall), programResult)
    }
}
