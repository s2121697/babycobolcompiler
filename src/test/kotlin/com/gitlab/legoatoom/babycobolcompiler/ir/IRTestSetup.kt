package com.gitlab.legoatoom.babycobolcompiler.ir

import com.gitlab.legoatoom.babycobolcompiler.checker.SymbolTable
import com.gitlab.legoatoom.babycobolcompiler.util.whenNotNull

internal open class IRTestSetup {
    protected fun getExpectedOutput(varDecls: String? = null, stats: String = "", paragraphs: String = "", paragraphsCall: String = "   "): String {
        val scope = "val scope : java.util.Stack<Set<String>> = java.util.Stack()"

        return buildString {
            appendLine("// Scope Stack")
            appendLine(scope)
            appendLine("\n// Declare Variables")
            if(varDecls != null) append("$varDecls\n") else append("")
            appendLine("\n// Begin Code")
            appendLine("try {")
            append(stats)
            appendLine("\n\n\n\n// Paragraphs Call")
            append(paragraphsCall)
            appendLine("\n} catch (ignore: ExitScript) {}")
            appendLine("\n\n// Paragraphs Definition")
            append(paragraphs)

            appendLine("\n\n// Boilerplate")
            append(boilerplate)
        }
    }

    protected fun getProgramResultFromSentences(
        sentences: List<IRSentence> = emptyList(),
        paragraphs: List<IRParagraph> = emptyList(),
        symbolTable: SymbolTable = SymbolTable()
    ): String {
        val procedure = IRProcedureDivision(sentences, paragraphs)
        return IRProgram(symbolTable, procedure, paragraphs = paragraphs).convert()
    }

    private val boilerplate = """
        open class Picture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) {
            private var value: String = padSymbol.toString().repeat(len)
            
            open fun getValue(): Any { return this.value }
            
            override fun toString(): String { return this.value }
            
            fun setValue(newVal: String) {
                // Check length of newVal. Pad to left with spaces / 0's or cut left most part of string.
                var updatedVal: String = newVal
                if (newVal.length < len) {
                    updatedVal = newVal.padStart(len, padSymbol)
                } else if (newVal.length > len) {
                    updatedVal = newVal.takeLast(len)
                }

                // Check if value correspond with regex
                if (regex.matches(updatedVal)) this.value = updatedVal
                else {
                    val errMessage = "Cannot assign " + updatedVal + " to data entry " + this.name
                    throw Exception(errMessage)
                }
            }
        }
        
        class IntPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
            override fun getValue(): java.math.BigInteger {
                return super.getValue().toString().toBigInteger()
            }
        }
        
        class StringPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
            override fun getValue(): String {
                return super.getValue().toString()
            }
        }
        
        class GoToReturn : Throwable()
        class ExitScript : Throwable()
    """.trimIndent()
}