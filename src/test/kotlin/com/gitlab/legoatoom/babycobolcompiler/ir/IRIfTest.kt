package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRIfTest : IRTestSetup() {
    @Test
    fun onlyIf() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(negation = true, boolExprs = listOf(IRBoolExpr(falseLiteral = true))),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"Hello World!\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (!false) {
                println("Hello World!")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun simpleIfElse() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(trueLiteral = true),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"false\""))), false)
                    )
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (true) {
                println("true")
            } else {
                println("false")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    // Indentation with multiple statements is not preserved, but this is no problem.
    @Test
    fun multipleStatementsInBlocksIfElse() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(trueLiteral = true),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false),
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"Hello World!\""))), false)
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"false\""))), false),
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"Bye World!\""))), false)
                    )
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (true) {
                println("true")
            println("Hello World!")
            } else {
                println("false")
            println("Bye World!")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun simpleIfGTExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = true, eq = false, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                            IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false)
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger() > 9.toBigInteger()) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun simpleIfGEExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = false, ge = true, lt = false, le = false),
                        arithExprs = listOf(
                            IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                            IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false)
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger() >= 9.toBigInteger()) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun simpleIfLTExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = false, ge = false, lt = true, le = false),
                        arithExprs = listOf(
                            IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                            IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false)
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger() < 9.toBigInteger()) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun simpleIfLEExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = false, ge = false, lt = false, le = true),
                        arithExprs = listOf(
                            IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                            IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false)
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger() <= 9.toBigInteger()) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun simpleIfEQExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = true, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                            IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false)
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger() == 9.toBigInteger()) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun arithExpressionIfEQExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = true, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRMulDivExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                true
                            ),
                            IRAddSubExpr(
                                IRAddSubExpr(
                                    IRArithAtomic(IRLiteral.IRIntLit(1.toBigInteger()), false),
                                    IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                    true
                                ),
                                IRMulDivExpr(
                                    IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                    IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                    true
                                ),
                                true,
                            ),
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger().times(9.toBigInteger()) == 1.toBigInteger().plus(10.toBigInteger()).plus(9.toBigInteger().times(10.toBigInteger()))) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun arithPowerIfEQExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = true, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRPowerExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false)
                            ),
                            IRPowerExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false)
                            ),
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger().pow(9.toBigInteger().toInt()) == 9.toBigInteger().pow(10.toBigInteger().toInt())) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun arithAddIfEQExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = true, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRAddSubExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                true
                            ),
                            IRAddSubExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                true
                            ),
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger().plus(9.toBigInteger()) == 9.toBigInteger().plus(10.toBigInteger())) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun arithSubIfEQExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = true, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRAddSubExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                false
                            ),
                            IRAddSubExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                false
                            ),
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger().minus(9.toBigInteger()) == 9.toBigInteger().minus(10.toBigInteger())) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun arithMulIfEQExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = true, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRMulDivExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                true
                            ),
                            IRMulDivExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                true
                            ),
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger().times(9.toBigInteger()) == 9.toBigInteger().times(10.toBigInteger())) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun arithDivIfEQExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = false, eq = true, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRMulDivExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                false
                            ),
                            IRMulDivExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(10.toBigInteger()), false),
                                false
                            ),
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (10.toBigInteger().div(9.toBigInteger()) == 9.toBigInteger().div(10.toBigInteger())) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun andBoolExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        boolOperator = IRBoolOp(and = true, xor = false, or = false),
                        boolExprs = listOf(
                            IRBoolExpr(trueLiteral = true),
                            IRBoolExpr(negation = true, boolExprs = listOf(IRBoolExpr(falseLiteral = true)))
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (true && !false) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun orBoolExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        boolOperator = IRBoolOp(and = false, xor = false, or = true),
                        boolExprs = listOf(
                            IRBoolExpr(trueLiteral = true),
                            IRBoolExpr(falseLiteral = true)
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (true || false) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun xorBoolExpr() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        boolOperator = IRBoolOp(and = false, xor = true, or = false),
                        boolExprs = listOf(
                            IRBoolExpr(trueLiteral = true),
                            IRBoolExpr(negation = true, boolExprs = listOf(IRBoolExpr(falseLiteral = true)))
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (true xor !false) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun combineArithExprBoolOp() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        boolOperator = IRBoolOp(and = true, xor = false, or = false),
                        boolExprs = listOf(
                            IRBoolExpr(
                                compOperator = IRCompOp(eq = false, gt = false, ge = true, lt = false, le = false),
                                arithExprs = listOf(
                                    IRArithAtomic(IRLiteral.IRIntLit(36.toBigInteger()), false),
                                    IRAddSubExpr(
                                        IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                        IRArithAtomic(IRLiteral.IRIntLit(6.toBigInteger()), false),
                                        true
                                    )
                                )
                            ),
                            IRBoolExpr(
                                compOperator = IRCompOp(eq = true, gt = false, ge = false, lt = false, le = false),
                                arithExprs = listOf(
                                    IRAddSubExpr(
                                        IRArithAtomic(IRLiteral.IRIntLit(9.toBigInteger()), false),
                                        IRArithAtomic(IRLiteral.IRIntLit(1.toBigInteger()), false),
                                        false
                                    ),
                                    IRArithAtomic(IRLiteral.IRIntLit(8.toBigInteger()), false),
                                )
                            )
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (36.toBigInteger() >= 9.toBigInteger().plus(6.toBigInteger()) && 9.toBigInteger().minus(1.toBigInteger()) == 8.toBigInteger()) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun arithExprPrecedence() {
        val sentences = IRSentence(
            listOf(
                IRIf(
                    IRBoolExpr(
                        compOperator = IRCompOp(eq = false, gt = true, ge = false, lt = false, le = false),
                        arithExprs = listOf(
                            IRAddSubExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(5.toBigInteger()), false),
                                IRMulDivExpr(
                                    IRArithAtomic(IRLiteral.IRIntLit(5.toBigInteger()), false),
                                    IRPowerExpr(
                                        IRArithAtomic(IRLiteral.IRIntLit(5.toBigInteger()), false),
                                        IRArithAtomic(IRLiteral.IRIntLit(5.toBigInteger()), false)
                                    ),
                                    true
                                ),
                                true
                            ),
                            IRArithAtomic(IRLiteral.IRIntLit(50.toBigInteger()), false),
                        )
                    ),
                    listOf(
                        IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"true\""))), false)
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            if (5.toBigInteger().plus(5.toBigInteger().times(5.toBigInteger().pow(5.toBigInteger().toInt()))) > 50.toBigInteger()) {
                println("true")
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }
}
