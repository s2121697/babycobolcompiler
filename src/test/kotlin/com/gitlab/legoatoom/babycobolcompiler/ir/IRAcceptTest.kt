package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRAcceptTest : IRTestSetup() {

    @Test
    fun simpleAccept() {
        val sentences = IRSentence(listOf(IRAccept(listOf(IRIdentifier("Hello", "")))))
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _hello.setValue(readln())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun multiAccept() {
        val sentences = IRSentence(
            listOf(
                IRAccept(
                    listOf(
                        IRIdentifier("A", ""),
                        IRIdentifier("B", ""),
                        IRIdentifier("C", ""),
                        IRIdentifier("D", ""),
                        IRIdentifier("E", "")
                    )
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _a.setValue(readln())
            _b.setValue(readln())
            _c.setValue(readln())
            _d.setValue(readln())
            _e.setValue(readln())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }
}
