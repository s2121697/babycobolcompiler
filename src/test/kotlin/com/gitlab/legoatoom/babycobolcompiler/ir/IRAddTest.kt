package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRAddTest : IRTestSetup() {
    @Test
    fun simpleAdd() {
        val sentences = IRSentence(
            listOf(
                IRAdd(
                    IRLiteral.IRIntLit(6.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""), false, false
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((6.toBigInteger().plus(9.toBigInteger())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun multipleAdds() {
        val sentences = IRSentence(
            listOf(
                IRAdd(
                    IRLiteral.IRIntLit(6.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""), false, false,
                ),
                IRAdd(
                    IRLiteral.IRIntLit(3500.toBigInteger()),
                    IRIdentifier("TEST", ""),
                    null, false, true,
                ),
                IRAdd(
                    IRLiteral.IRIntLit(3.toBigInteger()),
                    IRIdentifier("TEST", ""),
                    null, false, true,
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((6.toBigInteger().plus(9.toBigInteger())).toString())
            _test.setValue((3500.toBigInteger().plus(_test.getValue())).toString())
            _test.setValue((3.toBigInteger().plus(_test.getValue())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }
}
