package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRDisplayTest : IRTestSetup() {

    @Test
    fun simpleDisplay() {
        val sentences = IRSentence(
            listOf(
                IRDisplay(
                    listOf(IRDispExpr(IRLiteral.IRIntLit(8.toBigInteger()))),
                    false
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            println(8)
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun simpleVariableDisplay() {
        val sentences = IRSentence(
            listOf(
                IRAdd(
                    IRLiteral.IRIntLit(100.toBigInteger()),
                    IRLiteral.IRIntLit(250.toBigInteger()),
                    IRIdentifier("VAR", ""),
                    false, false
                ),
                IRDisplay(
                    listOf(IRDispExpr(IRIdentifier("VAR", ""))),
                    false
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _var.setValue((100.toBigInteger().plus(250.toBigInteger())).toString())
            println(_var)
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun displayMultipleAtomics() {
        val sentences = IRSentence(
            listOf(
                IRDisplay(
                    listOf(
                        IRDispExpr(IRLiteral.IRIntLit(8.toBigInteger())),
                        IRDispExpr(IRLiteral.IRIntLit(70.toBigInteger())),
                        IRDispExpr(IRLiteral.IRTxtLit("\"Hello\""))
                    ), false
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            println(8)
            println(70)
            println("Hello")
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun displayWithNoAdvancing() {
        val sentences = IRSentence(
            listOf(
                IRDisplay(
                    listOf(IRDispExpr(IRLiteral.IRIntLit(8.toBigInteger()))),
                    true
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            print(8)
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun displayDelimitedBySpace() {
        val sentences = IRSentence(
            listOf(
                IRDisplay(
                    listOf(
                        IRDispExpr(
                            IRLiteral.IRTxtLit("\"  Example with whitespace \""),
                            DelimiterType.SPACE
                        )
                    ),
                    false
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            println("Example with whitespace")
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun displayDelimitedByLiteral() {
        val sentences = IRSentence(
            listOf(
                IRDisplay(
                    listOf(
                        IRDispExpr(
                            IRLiteral.IRTxtLit("\"An example sentence delimited by a literal\""),
                            literal = IRLiteral.IRTxtLit("\"d\"")
                        )
                    ),
                    false
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            println("An example sentence ")
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun displayDelimitedByLiteralMultiple() {
        val sentences = IRSentence(
            listOf(
                IRDisplay(
                    listOf(
                        IRDispExpr(
                            IRLiteral.IRTxtLit("\"An example sentence delimited by a literal\""),
                            literal = IRLiteral.IRTxtLit("\"d\"")
                        ),
                        IRDispExpr(
                            IRLiteral.IRTxtLit("\"Second string\""),
                            literal = IRLiteral.IRTxtLit("\"t\"")
                        )
                    ),
                    false
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            println("An example sentence ")
            println("Second s")
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun displayDelimitedByLiteralMultipleNoAdvancing() {
        val sentences = IRSentence(
            listOf(
                IRDisplay(
                    listOf(
                        IRDispExpr(
                            IRLiteral.IRTxtLit("\"An example sentence delimited by a literal\""),
                            literal = IRLiteral.IRTxtLit("\"d\"")
                        ),
                        IRDispExpr(
                            IRLiteral.IRTxtLit("\"Second string\""),
                            literal = IRLiteral.IRTxtLit("\"t\"")
                        )
                    ),
                    true
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            print("An example sentence ")
            print("Second s")
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun withNoAdvancingTypo() {
        val sentences = IRSentence(
            listOf(
                IRDisplay(
                    listOf(
                        IRDispExpr(IRLiteral.IRIntLit(900.toBigInteger())),
                        IRDispExpr(IRIdentifier("WIT", "")),
                        IRDispExpr(IRIdentifier("NO", "")),
                        IRDispExpr(IRIdentifier("ADVANCING", "")),
                    ),
                    false
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            println(900)
            println(_wit)
            println(_no)
            println(_advancing)
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)

    }
}
