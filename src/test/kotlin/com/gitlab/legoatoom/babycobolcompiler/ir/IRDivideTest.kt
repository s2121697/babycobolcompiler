package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRDivideTest : IRTestSetup() {
    @Test
    fun simpleDivideWithoutRemainder() {
        val sentences = IRSentence(
            listOf(
                IRDivide(
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""), null, false, false,
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((9.toBigInteger().div(9.toBigInteger())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun simpleDivideWithRemainder() {
        val sentences = IRSentence(
            listOf(
                IRDivide(
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""),  IRIdentifier("TEST2", ""), false, false,
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((9.toBigInteger().div(9.toBigInteger())).toString())
            _test2.setValue((9.toBigInteger().mod(9.toBigInteger())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun multipleMultiply() {
        val sentences = IRSentence(
            listOf(
                IRDivide(
                    IRLiteral.IRIntLit(60.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""), null,false, false,
                ),
                IRDivide(
                    IRIdentifier("TEST", ""),
                    IRLiteral.IRIntLit(3500.toBigInteger()),
                    null, null, true, false,
                ),
                IRDivide(
                    IRIdentifier("TEST", ""),
                    IRLiteral.IRIntLit(3.toBigInteger()),
                    null, null, true, false,
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((60.toBigInteger().div(9.toBigInteger())).toString())
            _test.setValue((_test.getValue().div(3500.toBigInteger())).toString())
            _test.setValue((_test.getValue().div(3.toBigInteger())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }
}
