package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IREvaluateTest : IRTestSetup() {
    @Test
    fun evalArithTest() {
        val sentences = IRSentence(
            listOf(
                IREvaluate(
                    IRAddSubExpr(
                        IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                        IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                        true
                    ),
                    listOf(
                        IRWhenBlock(
                            listOf(IRLiteral.IRIntLit(6.toBigInteger())), listOf(
                                IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"TRUE\""))), false),
                                IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"FALSE\""))), false)
                            )
                        )
                    )
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            when (3.toBigInteger().plus(3.toBigInteger())) {
                6.toBigInteger() -> {
                    println("TRUE")
                    println("FALSE")
                }
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun evalBoolTest() {
        val sentences = IRSentence(
            listOf(
                IREvaluate(
                    IRBoolExpr(
                        compOperator = IRCompOp(gt = true, eq = false, ge = false, le = false, lt = false),
                        arithExprs = listOf(
                            IRAddSubExpr(
                                IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                                IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                                true
                            ),
                            IRArithAtomic(IRLiteral.IRIntLit(6.toBigInteger()), false)
                        )
                    ),
                    listOf(
                        IRWhenBlock(
                            listOf(IRLiteral.IRTrueLit), listOf(
                                IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"TRUE\""))), false)
                            )
                        ),
                        IRWhenBlock(
                            emptyList(), listOf(
                                IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"FALSE\""))), false)
                            )
                        )
                    )
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            when (3.toBigInteger().plus(3.toBigInteger()) > 6.toBigInteger()) {
                true -> {
                    println("TRUE")
                }
                else -> {
                    println("FALSE")
                }
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun otherTest() {
        val sentences = IRSentence(
            listOf(
                IREvaluate(
                    IRAddSubExpr(
                        IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                        IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                        true
                    ),
                    listOf(
                        IRWhenBlock(
                            listOf(IRLiteral.IRIntLit(6.toBigInteger())), listOf(
                                IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"TRUE\""))), false)
                            )
                        ),
                        IRWhenBlock(
                            emptyList(), listOf(
                                IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"FALSE\""))), false)
                            )
                        )
                    )
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            when (3.toBigInteger().plus(3.toBigInteger())) {
                6.toBigInteger() -> {
                    println("TRUE")
                }
                else -> {
                    println("FALSE")
                }
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun emptyTest() {
        val sentences = IRSentence(
            listOf(
                IREvaluate(
                    IRAddSubExpr(
                        IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                        IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                        true
                    ),
                    emptyList()
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            3.toBigInteger().plus(3.toBigInteger())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun multiConditions() {
        val sentences = IRSentence(
            listOf(
                IREvaluate(
                    IRAddSubExpr(
                        IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                        IRArithAtomic(IRLiteral.IRIntLit(3.toBigInteger()), false),
                        true
                    ),
                    listOf(
                        IRWhenBlock(
                            listOf(
                                IRLiteral.IRIntLit(35.toBigInteger()),
                                IRLiteral.IRIntLit(32.toBigInteger())
                            ),
                            listOf(
                                IRDisplay(listOf(IRDispExpr(IRLiteral.IRTxtLit("\"Hello\""))), false)
                            )
                        )
                    )
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            when (3.toBigInteger().plus(3.toBigInteger())) {
                35.toBigInteger(), 32.toBigInteger() -> {
                    println("Hello")
                }
            }
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }
}
