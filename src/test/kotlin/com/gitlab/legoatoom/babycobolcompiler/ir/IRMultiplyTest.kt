package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRMultiplyTest : IRTestSetup() {
    @Test
    fun simpleMultiply() {
        val sentences = IRSentence(
            listOf(
                IRMultiply(
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""), false, false,
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((9.toBigInteger().times(9.toBigInteger())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun multipleMultiply() {
        val sentences = IRSentence(
            listOf(
                IRMultiply(
                    IRLiteral.IRIntLit(60.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""), false, false,
                ),
                IRMultiply(
                    IRLiteral.IRIntLit(3500.toBigInteger()),
                    IRIdentifier("TEST", ""),
                    null, false, true,
                ),
                IRMultiply(
                    IRLiteral.IRIntLit(3.toBigInteger()),
                    IRIdentifier("TEST", ""),
                    null, false, true,
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((60.toBigInteger().times(9.toBigInteger())).toString())
            _test.setValue((3500.toBigInteger().times(_test.getValue())).toString())
            _test.setValue((3.toBigInteger().times(_test.getValue())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }
}
