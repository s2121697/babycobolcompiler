package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRSubtractTest : IRTestSetup() {
    @Test
    fun simpleSubtract() {
        val sentences = IRSentence(
            listOf(
                IRSubtract(
                    IRLiteral.IRIntLit(6.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""), false, false,
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((9.toBigInteger().minus(6.toBigInteger())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }

    @Test
    fun multipleSubtracts() {
        val sentences = IRSentence(
            listOf(
                IRSubtract(
                    IRLiteral.IRIntLit(6.toBigInteger()),
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRIdentifier("TEST", ""), false, false,
                ),
                IRSubtract(
                    IRLiteral.IRIntLit(3500.toBigInteger()),
                    IRIdentifier("TEST", ""),
                    null, false, true,
                ),
                IRSubtract(
                    IRLiteral.IRIntLit(3.toBigInteger()),
                    IRIdentifier("TEST", ""),
                    null, false, true,
                )
            )
        )
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            _test.setValue((9.toBigInteger().minus(6.toBigInteger())).toString())
            _test.setValue((_test.getValue().minus(3500.toBigInteger())).toString())
            _test.setValue((_test.getValue().minus(3.toBigInteger())).toString())
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }
}
