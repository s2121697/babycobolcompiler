package com.gitlab.legoatoom.babycobolcompiler.ir

import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRStopTest : IRTestSetup() {
    @Test
    fun simpleStop() {
        val sentences = IRSentence(listOf(IRStop()))
        val programResult = getProgramResultFromSentences(listOf(sentences))

        val stats = """
            throw ExitScript()
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats), programResult)
    }
}
