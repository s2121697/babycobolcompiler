package com.gitlab.legoatoom.babycobolcompiler.ir

import com.gitlab.legoatoom.babycobolcompiler.checker.SymbolTable
import com.gitlab.legoatoom.babycobolcompiler.util.IdentifierInfo
import kotlin.test.Test
import kotlin.test.assertEquals

internal class IRDataDivisionTest : IRTestSetup() {
    @Test
    fun onlyIdDivision() {
        val programResult = getProgramResultFromSentences()

        assertEquals(getExpectedOutput(), programResult)
    }

    @Test
    fun stringPictureClause() {
        val symbolTable = SymbolTable(
            identifiers = linkedMapOf(
                listOf("TEST") to IdentifierInfo(1, 1, Regex("^.{3}${'$'}"), ' ', 3)
            )
        )
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _test = StringPicture(Regex("^.{3}${'$'}"), 3, ' ', "_test")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun intPictureClause() {
        val symbolTable = SymbolTable(
            identifiers = linkedMapOf(
                listOf("TEST") to IdentifierInfo(1, 1, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int")
            )
        )
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _test = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_test")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun nestedDataEntriesPictureClause() {
        val symbolTable = SymbolTable(
            identifiers = linkedMapOf(
                listOf("TEST") to IdentifierInfo(1, 1),
                listOf("N", "TEST") to IdentifierInfo(2, 2),
                listOf("PIC", "N", "TEST") to IdentifierInfo(3, 3, Regex("^.{5}${'$'}"), ' ', 5)
            )
        )
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _picofnoftest = StringPicture(Regex("^.{5}${'$'}"), 5, ' ', "_picofnoftest")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun simpleArray() {
        val symbolTable = SymbolTable(
            identifiers = linkedMapOf(
                listOf("TEST") to IdentifierInfo(1, 1, Regex("^[0-9]{2}${'$'}"), '0', 2, returnType = "Int", occurs = 5)
            )
        )
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            val _test = Array(5) { IntPicture(Regex("^[0-9]{2}${'$'}"), 2, '0', "_test") }
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun nestedArray() {
        val symbolTable = SymbolTable(
            identifiers = linkedMapOf(
                listOf("X") to IdentifierInfo(1, 1, occurs = 5),
                listOf("Y", "X") to IdentifierInfo(5, 2, occurs = 7),
                listOf("Z", "Y", "X") to IdentifierInfo(9, 3, Regex("^.{5}${'$'}"), ' ', 5, occurs = 10)
            )
        )
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            val _zofyofx = Array(5) { Array(7) { Array(10) { StringPicture(Regex("^.{5}${'$'}"), 5, ' ', "_zofyofx") } } }
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun nestedArrayIndices() {
        val symbolTable = SymbolTable(
            identifiers = linkedMapOf(
                listOf("X") to IdentifierInfo(1, 1, occurs = 5),
                listOf("Y", "X") to IdentifierInfo(5, 2, occurs = 7),
                listOf("Z", "Y", "X") to IdentifierInfo(
                    9,
                    3,
                    Regex("^[0-9]{4}${'$'}"),
                    '0',
                    4,
                    returnType = "Int",
                    occurs = 10
                )
            )
        )
        val sentences = IRSentence(
            listOf(
                IRAdd(
                    IRLiteral.IRIntLit(9.toBigInteger()),
                    IRLiteral.IRIntLit(6.toBigInteger()),
                    IRIdentifier("zofyofx", "[0][2][9]"),
                    false, false
                ),
                IRDisplay(
                    listOf(
                        IRDispExpr(IRIdentifier("zofyofx", "[0][2][9]"))
                    ), false
                ),
                IRDisplay(
                    listOf(
                        IRDispExpr(IRIdentifier("zofyofx", "[0][2][0]"))
                    ), false
                )
            )
        )
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable, sentences = listOf(sentences))

        val varDecls = """
            val _zofyofx = Array(5) { Array(7) { Array(10) { IntPicture(Regex("^[0-9]{4}${'$'}"), 4, '0', "_zofyofx") } } }
        """.trimIndent()

        val stats = """
            _zofyofx[0][2][9].setValue((9.toBigInteger().plus(6.toBigInteger())).toString())
            println(_zofyofx[0][2][9])
            println(_zofyofx[0][2][0])
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats, varDecls = varDecls), programResult)
    }

    @Test
    fun simpleLike() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("X") to IdentifierInfo(1, 1, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("Y") to IdentifierInfo(1, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int")
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _x = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_x")
            var _y = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_y")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun likeDoesNotTakeOccurs() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("X") to IdentifierInfo(1, 1, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int", occurs = 5),
            listOf("Y") to IdentifierInfo(1, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int")
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            val _x = Array(5) { IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_x") }
            var _y = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_y")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun occursFollowLike() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("X") to IdentifierInfo(1, 1, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("Y") to IdentifierInfo(1, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int", occurs = 5)
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _x = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_x")
            val _y = Array(5) { IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_y") }
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun likeOnARecord() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("X") to IdentifierInfo(1, 1),
            listOf("Y", "X") to IdentifierInfo(5, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("M") to IdentifierInfo(1, 3),
            listOf("Y", "M") to IdentifierInfo(5, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _yofx = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_yofx")
            var _yofm = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_yofm")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun likeSufficientQualification() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("TEST") to IdentifierInfo(1, 1),
            listOf("X", "TEST") to IdentifierInfo(5, 2),
            listOf("Y", "X", "TEST") to IdentifierInfo(9, 3, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("X") to IdentifierInfo(1, 4, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("M") to IdentifierInfo(1, 5, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("N", "TEST") to IdentifierInfo(1, 6),
            listOf("Y", "N", "TEST") to IdentifierInfo(5, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _yofxoftest = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_yofxoftest")
            var _x = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_x")
            var _m = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_m")
            var _yofnoftest = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_yofnoftest")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun likeOnARecordMultipleChildren() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("X") to IdentifierInfo(1, 1),
            listOf("Y", "X") to IdentifierInfo(5, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("Z", "X") to IdentifierInfo(5, 3, Regex("^.{5}${'$'}"), ' ', 5),
            listOf("M") to IdentifierInfo(1, 4),
            listOf("Y", "M") to IdentifierInfo(5, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("Z", "M") to IdentifierInfo(5, 3, Regex("^.{5}${'$'}"), ' ', 5),
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _yofx = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_yofx")
            var _zofx = StringPicture(Regex("^.{5}${'$'}"), 5, ' ', "_zofx")
            var _yofm = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_yofm")
            var _zofm = StringPicture(Regex("^.{5}${'$'}"), 5, ' ', "_zofm")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun likeOnARecordMultipleChildrenAndOccurs() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("X") to IdentifierInfo(1, 1),
            listOf("Y", "X") to IdentifierInfo(5, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("Z", "X") to IdentifierInfo(5, 3, occurs = 10),
            listOf("Z1", "Z", "X") to IdentifierInfo(9, 4, Regex("^.{5}${'$'}"), ' ', 5, occurs = 5),
            listOf("M") to IdentifierInfo(1, 1),
            listOf("Y", "M") to IdentifierInfo(5, 2, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("Z", "M") to IdentifierInfo(5, 3, occurs = 10),
            listOf("Z1", "Z", "M") to IdentifierInfo(9, 4, Regex("^.{5}${'$'}"), ' ', 5, occurs = 5)
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _yofx = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_yofx")
            val _z1ofzofx = Array(10) { Array(5) { StringPicture(Regex("^.{5}${'$'}"), 5, ' ', "_z1ofzofx") } }
            var _yofm = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_yofm")
            val _z1ofzofm = Array(10) { Array(5) { StringPicture(Regex("^.{5}${'$'}"), 5, ' ', "_z1ofzofm") } }
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }

    @Test
    fun sufficientQualification() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("A") to IdentifierInfo(1, 1),
            listOf("B", "A") to IdentifierInfo(3, 2),
            listOf("C", "B", "A") to IdentifierInfo(5, 3),
            listOf("D", "C", "B", "A") to IdentifierInfo(7, 4, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("E", "B", "A") to IdentifierInfo(5, 5),
            listOf("D", "E", "B", "A") to IdentifierInfo(7, 6, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("F", "E", "B", "A") to IdentifierInfo(7, 7, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int")
        ))
        val sentences = IRSentence(listOf(
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofcofbofa", ""))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofcofbofa", ""))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofcofbofa", ""))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofeofbofa", ""))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofeofbofa", ""))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofeofbofa", ""))
            ), false)
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable, sentences = listOf(sentences))

        val varDecls = """
                var _dofcofbofa = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_dofcofbofa")
                var _dofeofbofa = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_dofeofbofa")
                var _fofeofbofa = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_fofeofbofa")
            """.trimIndent()

        val stats = """
                println(_dofcofbofa)
                println(_dofcofbofa)
                println(_dofcofbofa)
                println(_dofeofbofa)
                println(_dofeofbofa)
                println(_dofeofbofa)
            """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats, varDecls = varDecls), programResult)
    }

    @Test
    fun sufficientQualificationWithOccurs() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("A") to IdentifierInfo(1, 1),
            listOf("B", "A") to IdentifierInfo(3, 2),
            listOf("C", "B", "A") to IdentifierInfo(5, 3, occurs = 2),
            listOf("D", "C", "B", "A") to IdentifierInfo(7, 4, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int", occurs = 5),
            listOf("E", "B", "A") to IdentifierInfo(5, 5),
            listOf("D", "E", "B", "A") to IdentifierInfo(7, 6, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("F", "E", "B", "A") to IdentifierInfo(7, 7, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int")
        ))
        val sentences = IRSentence(listOf(
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofcofbofa", "[0][0]"))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofcofbofa", "[1][2]"))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofcofbofa", "[0][4]"))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofeofbofa", ""))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofeofbofa", ""))
            ), false),
            IRDisplay(listOf(
                IRDispExpr(IRIdentifier("dofeofbofa", ""))
            ), false)
        ))
        val programResult = getProgramResultFromSentences(symbolTable = symbolTable, sentences = listOf(sentences))

        val varDecls = """
            val _dofcofbofa = Array(2) { Array(5) { IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_dofcofbofa") } }
            var _dofeofbofa = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_dofeofbofa")
            var _fofeofbofa = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_fofeofbofa")
        """.trimIndent()

        val stats = """
            println(_dofcofbofa[0][0])
            println(_dofcofbofa[1][2])
            println(_dofcofbofa[0][4])
            println(_dofeofbofa)
            println(_dofeofbofa)
            println(_dofeofbofa)
        """.trimIndent()

        assertEquals(getExpectedOutput(stats = stats, varDecls = varDecls), programResult)
    }

    @Test
    fun sufficientQualificationInLike() {
        val symbolTable = SymbolTable(identifiers = linkedMapOf(
            listOf("A") to IdentifierInfo(1, 1),
            listOf("B", "A") to IdentifierInfo(3, 2),
            listOf("C", "B", "A") to IdentifierInfo(5, 3),
            listOf("D", "C", "B", "A") to IdentifierInfo(7, 4, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("E", "B", "A") to IdentifierInfo(5, 5),
            listOf("D", "E", "B", "A") to IdentifierInfo(7, 6, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("F", "E", "B", "A") to IdentifierInfo(7, 7, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("G") to IdentifierInfo(7, 6, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            listOf("H") to IdentifierInfo(7, 4, Regex("^[0-9]{3}${'$'}"), '0', 3, returnType = "Int"),
            ))

        val programResult = getProgramResultFromSentences(symbolTable = symbolTable)

        val varDecls = """
            var _dofcofbofa = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_dofcofbofa")
            var _dofeofbofa = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_dofeofbofa")
            var _fofeofbofa = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_fofeofbofa")
            var _g = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_g")
            var _h = IntPicture(Regex("^[0-9]{3}${'$'}"), 3, '0', "_h")
        """.trimIndent()

        assertEquals(getExpectedOutput(varDecls = varDecls), programResult)
    }
}