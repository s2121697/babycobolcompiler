package com.gitlab.legoatoom.babycobolcompiler.ir

import com.gitlab.legoatoom.babycobolcompiler.antlr.AntlrState
import com.gitlab.legoatoom.babycobolcompiler.builder.BuilderState
import com.gitlab.legoatoom.babycobolcompiler.checker.CheckerState
import com.gitlab.legoatoom.babycobolcompiler.exceptions.*
import com.gitlab.legoatoom.babycobolcompiler.runner.RunnerState
import com.gitlab.legoatoom.babycobolcompiler.state.BabyCobolCompilerState
import org.antlr.v4.runtime.tree.ParseTree
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Timeout
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import kotlin.reflect.KMutableProperty0
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Ignore

internal class IRPerformTest{

    private val outContent: ByteArrayOutputStream = ByteArrayOutputStream()
    private val errContent: ByteArrayOutputStream = ByteArrayOutputStream()
    private val originalOut: PrintStream = System.out
    private val originalErr: PrintStream = System.err

    @BeforeTest
    fun setUpStreams() {
        System.setOut(PrintStream(outContent))
        System.setErr(PrintStream(errContent))
    }

    @AfterTest
    fun restoreStreams() {
        System.setOut(originalOut)
        System.setErr(originalErr)
    }

    /**
     * Full Test
     */
    @Test
    @Ignore
    @Timeout(100)
    fun performGotoTest(){
        val code = """
 IDENTIFICATION DIVISION.
 PROGRAM-ID. PERFORM-GO-TO.
 PROCEDURE DIVISION.
 A.
     DISPLAY "A" WITH NO ADVANCING.
     PERFORM B.
     PERFORM B THROUGH C.
     PERFORM D THROUGH E.
     PERFORM C THROUGH D.
     DISPLAY "A" WITH NO ADVANCING.
 B.
     DISPLAY "B" WITH NO ADVANCING.
 C.
     DISPLAY "C" WITH NO ADVANCING.
 D.
     DISPLAY "D" WITH NO ADVANCING.
     GO TO E.
     DISPLAY "D" WITH NO ADVANCING.
 E.
     DISPLAY "E" WITH NO ADVANCING.
 F.
     DISPLAY "F" WITH NO ADVANCING.
     STOP.
        """.trimIndent()

        val expected: String = """
            // Scope Stack
            val scope : java.util.Stack<Set<String>> = java.util.Stack()
            
            // Declare Variables
            
            // Begin Code
            try {
            
            
            
            
            // Paragraphs Call
               _a();_b();_c();_d();_e();_f()
            } catch (ignore: ExitScript) {}
            
            
            // Paragraphs Definition
            fun _a(){
               print("A")
               try{scope.push(setOf("B")); _b()}catch(ignore: GoToReturn){}finally{scope.pop()}
               try{scope.push(setOf("B","C")); _b();_c()}catch(ignore: GoToReturn){}finally{scope.pop()}
               try{scope.push(setOf("D","E")); _d();_e()}catch(ignore: GoToReturn){}finally{scope.pop()}
               try{scope.push(setOf("C","D")); _c();_d()}catch(ignore: GoToReturn){}finally{scope.pop()}
               print("A")
            };fun _b(){
               print("B")
            };fun _c(){
               print("C")
            };fun _d(){
               print("D")
               _e();if (scope.peek().contains("E")) { throw GoToReturn()};_f();if (scope.peek().contains("F")) { throw GoToReturn()};throw ExitScript()
               print("D")
            };fun _e(){
               print("E")
            };fun _f(){
               print("F")
               throw ExitScript()
            }
            
            // Boilerplate
            open class Picture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) {
                private var value: String = padSymbol.toString().repeat(len)
                
                open fun getValue(): Any { return this.value }
                
                override fun toString(): String { return this.value }
                
                fun setValue(newVal: String) {
                    // Check length of newVal. Pad to left with spaces / 0's or cut left most part of string.
                    var updatedVal: String = newVal
                    if (newVal.length < len) {
                        updatedVal = newVal.padStart(len, padSymbol)
                    } else if (newVal.length > len) {
                        updatedVal = newVal.takeLast(len)
                    }
            
                    // Check if value correspond with regex
                    if (regex.matches(updatedVal)) this.value = updatedVal
                    else {
                        val errMessage = "Cannot assign " + updatedVal + " to data entry " + this.name
                        throw Exception(errMessage)
                    }
                }
            }
            
            class IntPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
                override fun getValue(): java.math.BigInteger {
                    return super.getValue().toString().toBigInteger()
                }
            }
            
            class StringPicture(private val regex: Regex, private val len: Int, private val padSymbol: Char, private val name: String) : Picture(regex, len, padSymbol, name) {
                override fun getValue(): String {
                    return super.getValue().toString()
                }
            }
            
            class GoToReturn : Throwable()
            class ExitScript : Throwable()
            """.trimIndent()
        var parseTree: ParseTree? = null

        val buildResult: String = (code
                //Stage 3
                stage ::AntlrState
                pipe ::validate
                apply { parseTree = it }

                //Stage 4
                stage ::CheckerState
                pipe ::validate
                to parseTree!!

                //Stage 5
                stage ::BuilderState
                pipe ::validate
                pipe IRProgram::convert
                )
        assertEquals(expected, buildResult)
        val runner = RunnerState(buildResult)
        when (val printResult = runner.handle()){
            is StateSuccess -> {
                assertEquals(buildString {
                    append("Running...")
                    append(System.lineSeparator())
                    append(System.lineSeparator())
                    append("ABBCDECDEF")
                },outContent.toString())
            }
            is StateException -> fail(printResult.exception.message)
            is StateFailure -> fail(printResult.failure.message)
            is StateError -> fail(printResult.error.message)
        }

    }

    private infix fun <T, C> T.pipe(func: (T) -> C): C = func(this)
    private infix fun <T> T.apply(func: (T) -> Unit): T = func(this).run { this@apply }
    private infix fun <T, C> T.stage(func: (T) -> BabyCobolCompilerState<C>): StateResult<out C> = func(this).handle()
    private infix fun <T> T.store(func: KMutableProperty0<T>): T = apply(func::set)
    private infix fun <T, C> T.retrieve(func: KMutableProperty0<C>): Pair<T, C> = this to func.get()
}
